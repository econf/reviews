<?php
/**
 * Created by IntelliJ IDEA.
 * User: joaoluis
 * Date: 16/05/16
 * Time: 23:38
 */

namespace EConf\Reviews;


use Eventy;
use SwotPHP\Facades\Native\Swot;

class Conflict {
    const MIN_SIMILARITY = 75;


    /**
     * @var array
     */
    private $author;
	/**
     * @var \App\User
     */
    private $reviewer;

	/**
     * @var ConflictException|null
     */
    private $conflict = null;


    /**
     * Conflict constructor.
     * @param $author
     * @param $reviewer
     */
    public function __construct( $author, $reviewer ) {
        $this->author = $author;
        $this->reviewer = $reviewer;

        $this->checkConflict();
    }

    public function hasConflict() {
        return !is_null( $this->conflict );
    }

    public function getConflict() {
        return $this->conflict;
    }

    private function checkConflict() {

        try {
            // Check name
            $this->checkName();

            // Check organization email
            $this->checkOrganizationEmail();

            Eventy::action('conflict.check', $this->author, $this->reviewer);

        } catch (ConflictException $e) {
            $this->conflict = $e;
        }

    }

    private function checkName() {
        similar_text( $this->author['name'], $this->reviewer->name, $similarity );
        if( $similarity > self::MIN_SIMILARITY ){
            throw new ConflictException('name', sprintf("%.2f%%", $similarity));
        }
    }

    private function checkOrganizationEmail() {
        $auth_email = $this->author['email'];
        if(Swot::isAcademic($auth_email)){
            $auth_domain = explode( '@', $auth_email )[1];
            $rev_domain = explode( '@', $this->reviewer->email )[1];

            if($auth_domain == $rev_domain){
                throw new ConflictException('org_email', Swot::schoolName($auth_domain));
            }
        }
    }

}
