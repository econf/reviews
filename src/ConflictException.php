<?php
/**
 * Created by IntelliJ IDEA.
 * User: joaoluis
 * Date: 16/05/16
 * Time: 23:45
 */

namespace EConf\Reviews;


use Exception;

class ConflictException extends Exception{

    private $type;

    /**
     * @inheritDoc
     */
    public function __construct( $type, $message ) {
        parent::__construct( $message );
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    public function getTitle(){
        return trans("reviews::reviews.conflict.type.{$this->type}.title", ['msg' => $this->getMessage()]);
    }

    public function getDisplayMessage(){
        return trans("reviews::reviews.conflict.type.{$this->type}.message", ['msg' => $this->getMessage()]);
    }


}
