<?php

namespace EConf\Reviews;

use App\Traits\HasDataFieldTrait;
use App\Traits\TenantableTrait;
use App\User;
use EConf\Submissions\Submission;
use Illuminate\Database\Eloquent\Model;
use Setting;

class Review extends Model
{
    use TenantableTrait;

    use HasDataFieldTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'submission_id',
        'data',
    ];

    protected $casts = [
        'data' => 'array',
    ];

    /**
     * @inheritDoc
     */
    protected static function boot() {
        parent::boot();

        self::updating( function ( Review $review ) {

            if(!empty( $review->data )) {
                $review->computeScore();
            }

        } );
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function submission(){
        return $this->belongsTo( Submission::class );
    }

    public function assignee(){
        return $this->belongsTo( User::class );
    }

    private function computeScore(){

        $fields = $this->data( 'field', [] )->all();

        $rf = ReviewHelpers::formFields();

        $total = 0;
        $sum_fields = 0;

        foreach ($rf as $f){
            if ( array_key_exists( $f['slug'], $fields ) ) {
                $total += $f['weight'];
                $sum_fields += $fields[$f['slug']] * $f['weight'];
            }
        }

        if($total == 0) {
            $score = 0;
        } else {
            $score = $sum_fields / $total;
        }

        $this->score = $score;

    }

}
