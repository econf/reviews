<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create( 'reviews', function ( Blueprint $table ) {
            $table->increments( 'id' );

            $table->integer( 'submission_id' )->unsigned();
            $table->foreign( 'submission_id' )->references( 'id' )->on( 'submissions' )->onDelete( 'cascade' );

            $table->integer( 'user_id' )->unsigned();
            $table->foreign( 'user_id' )->references( 'id' )->on( 'users' );

            // Subreviewer
            $table->integer( 'assignee_id' )->unsigned()->nullable()->default(null);
            $table->foreign( 'assignee_id' )->references( 'id' )->on( 'users' );

            // Multi-tenancy
            $table->integer( 'conference_id' )->unsigned()->nullable();
            $table->foreign( 'conference_id' )->references( 'id' )->on( 'conferences' )->onDelete( 'cascade' );

            // All the other data
            $table->longText( 'data' );

            $table->double('score')->nullable()->default(null);

            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop( 'reviews' );
    }
}
