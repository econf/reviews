<?php
/**
 * Created by IntelliJ IDEA.
 * User: joaoluis
 * Date: 18/04/16
 * Time: 20:19
 */

namespace EConf\Reviews;


use Auth;
use Carbon\Carbon;
use DB;
use EConf\Submissions\Submission;
use Setting;

class ReviewHelpers {

    public static function isOpen(){
        if(Setting::has('conf-rev-start_date') && Setting::has('conf-rev-end_date')){
            $start = Carbon::parse(Setting::get('conf-rev-start_date').' 00:00');
            $end = Carbon::parse(Setting::get('conf-rev-end_date').' 23:59:59');
            return Carbon::now()->between($start, $end);
        }
        return false;
    }

    public static function isAfter(){
        if(Setting::has('conf-rev-end_date')){
            $end = Carbon::parse(Setting::get('conf-rev-end_date').' 23:59:59');
            return Carbon::now()->gt($end);
        }
        return false;
    }

    public static function isBiddingOpen(){
        if(Setting::has('conf-rev-bid-start_date') && Setting::has('conf-rev-bid-end_date')){
            $start = Carbon::parse(Setting::get('conf-rev-bid-start_date').' 00:00');
            $end = Carbon::parse(Setting::get('conf-rev-bid-end_date').' 23:59:59');
            return Carbon::now()->between($start, $end);
        }
        return false;
    }

    public static function isAssignmentPhase(){
        if(Setting::has('conf-rev-bid-end_date') && Setting::has('conf-rev-start_date')){
            $start = Carbon::parse(Setting::get('conf-rev-bid-end_date').' 23:59:59');
            $end = Carbon::parse(Setting::get('conf-rev-start_date').' 00:00');
            return Carbon::now()->between($start, $end);
        }
        return false;
    }

    public static function isReviewer($user_id = null){
        if(is_null( $user_id )){
            $user_id = Auth::id();
        }
        return Review::where('user_id', $user_id)->orWhere('assignee_id', $user_id)->count() > 0;
    }

    public static function formFields(){
        return unserialize(Setting::get('conf-rev-form-fields', serialize([])));
    }

    public static function submissionsToAccept($submissions_count = null){

        if(is_null( $submissions_count )){
            $submissions_count = Submission::count();
        }

        if(Setting::get('conf-rev-acpt-type', 'percent') == "percent"){
            // Percent
            $percent = Setting::get('conf-rev-acpt-qty', 50);
            $total = $submissions_count;

            $to_accept = floor( ($percent / 100) * $total );
        } else {
            // Number
            $to_accept = Setting::get('conf-rev-acpt-qty', 50);
        }

        return $to_accept;
    }

}
