<?php

use EConf\Reviews\Http\Middleware\ReviewerMiddleware;

function rev_routes() {
    Route::group( [
        'namespace' => 'EConf\Reviews\Http\Controllers',
        'middleware' => [ 'web', 'locale' ]
    ], function () {

        Route::group( [ 'prefix' => 'admin', 'middleware' => 'committee:program' ], function () {
            // Admin routes

            Route::get( 'reviews/bidding', 'BiddingController@show' );
            Route::post( 'reviews/bidding', 'BiddingController@store' );

            Route::group( [ 'middleware' => 'chair:program' ], function () {

                Route::get( 'reviews/conflicts', 'ConflictController@show' );
                Route::post( 'reviews/conflicts', 'ConflictController@store' );
                Route::delete( 'reviews/conflicts', 'ConflictController@destroy' );

                Route::get( 'reviews/assignment/manual', 'AssignmentController@manual_show' );
                Route::post( 'reviews/assignment/manual', 'AssignmentController@manual_store' );
                Route::get( 'reviews/assignment/auto', 'AssignmentController@auto_show' );
                Route::get( 'reviews/assignment/auto/{algo}', 'AssignmentController@auto_algo' );
                Route::post( 'reviews/assignment/auto', 'AssignmentController@auto_store' );

                Route::get( 'reviews/overview', 'OverviewController@index' );
                Route::get( 'reviews/overview/{id}', 'OverviewController@show' )->where( [ 'id' => '[0-9]+' ] );

                //Program committee
                \EConf\Reviews\Http\Controllers\ProgramCommitteeController::routes();

                Route::get( 'settings/reviews', 'SettingsController@show' );
                Route::post( 'settings/reviews', 'SettingsController@store' );
            } );
        } );

        Route::group( [ 'prefix' => 'admin/reviews/review', 'middleware' => ['auth', ReviewerMiddleware::class ]], function () {
            Route::get( '/', 'ReviewsController@index' );
            Route::get( '{id}', 'ReviewsController@show' )->where( [ 'id' => '[0-9]+' ] );
            Route::post( '{id}', 'ReviewsController@store' )->where( [ 'id' => '[0-9]+' ] );
            Route::post( 'delegate', 'ReviewsController@delegate' );
        } );

    } );
}

Route::group( [ 'namespace' => 'EConf\Reviews\Http\Controllers', 'middleware' => [ 'web', 'locale' ] ], function () {

// Assets
    Route::get( 'assets/reviews.css', 'AssetsController@css' );
    Route::get( 'assets/reviews.js', 'AssetsController@js' );

} );

if ( Config::get( 'econf.multi' ) ) {
    // Multi
    Route::group( [ 'prefix' => '{conf_slug}', 'middleware' => 'multi' ], function () {
        rev_routes();
    } );
} else {
    // Single
    rev_routes();
}
