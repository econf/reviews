<?php

namespace EConf\Reviews\Http\Controllers;

use App\Committee;
use App\Http\Controllers\Controller;
use App\SessionType;
use Auth;
use Doctrine\Common\CommonException;
use EConf\Reviews\Bid;
use EConf\Reviews\Conflict;
use EConf\Reviews\ConflictException;
use EConf\Reviews\ReviewHelpers;
use EConf\Submissions\Submission;
use Illuminate\Http\Request;

use Setting;
use Flash;
use stdClass;

class ConflictController extends Controller{

    public function show(){

        $conflicts = [];

        $manual_conflicts = Bid::where('bid', 'conflict')->get();

        foreach ($manual_conflicts as $mc){
            $conflicts[] = $this->conflict_from_manual( $mc );
        }

        $subs = Submission::all();
        $revs = ProgramCommitteeController::getCommittee()->users;
        foreach ($subs as $sub){
            foreach ($revs as $rev){
                if(!$manual_conflicts->contains(function ($key, $value) use ($sub, $rev) {
                    return $value->submission_id == $sub->id && $value->user_id == $rev->id;
                })) {
                    foreach ( $sub->data( 'author' ) as $author ) {
                        $conf = new Conflict( $author, $rev );
                        if ( $conf->hasConflict() ) {
                            $conflicts[] = $this->conflict_from_auto( $sub, $author, $rev, $conf->getConflict() );
                        }
                    }
                }
            }
        }

        $submissions = Submission::orderBy('title')->get()->pluck('title', 'id');
        $reviewers = ProgramCommitteeController::getCommittee()->users()->orderBy('name')->get()->pluck('name', 'id');

        return view('reviews::conflict', compact('conflicts', 'submissions', 'reviewers'));

    }

    public function store(Request $request){

        $this->validate( $request, [
            'submission' => 'required|exists:submissions,id',
            'reviewer' => 'required|in:'.ProgramCommitteeController::getCommittee()->users->implode( 'id', ',' )
        ] );

        $bid = Bid::firstOrNew([
            'user_id' => $request->reviewer,
            'submission_id' => $request->submission
        ]);

        $bid->locked = true;
        $bid->bid = "conflict";

        $bid->save();

        Flash::success( trans('reviews::reviews.conflict.add_conflict_success') );

        return redirect(m_action( '\EConf\Reviews\Http\Controllers\ConflictController@show' ));

    }

    public function destroy(Request $request){
        if(!Committee::in('program', true)){
            abort(403);
        }

        $bid = Bid::findOrFail($request->bid);

        $bid->delete();

        Flash::success( trans('reviews::reviews.conflict.remove_conflict_success') );

        return redirect(m_action( '\EConf\Reviews\Http\Controllers\ConflictController@show' ));

    }

    private function conflict_from_manual($bid){
        $c = new stdClass();
        $c->submission = $bid->submission;
        $c->reviewer = $bid->user;
        $c->author = null;
        $c->type = $bid->locked?"manual_pc":"manual";
        $c->message = trans("reviews::reviews.conflict.type.{$c->type}.message");
        $c->bid = $bid;
        $c->title = trans("reviews::reviews.conflict.type.{$c->type}.title");
        return $c;
    }

    private function conflict_from_auto($submission, $author, $reviewer, ConflictException $exception){
        $c = new stdClass();
        $c->submission = $submission;
        $c->reviewer = $reviewer;
        $c->author = $author;
        $c->type = $exception->getType();
        $c->message = $exception->getDisplayMessage();
        $c->title = $exception->getTitle();
        return $c;
    }

}
