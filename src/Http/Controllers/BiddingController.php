<?php

namespace EConf\Reviews\Http\Controllers;

use App\Http\Controllers\Controller;
use App\SessionType;
use Auth;
use EConf\Reviews\Bid;
use EConf\Reviews\ReviewHelpers;
use EConf\Submissions\Submission;
use Illuminate\Http\Request;

use Setting;
use Flash;

class BiddingController extends Controller{

    public function show(){

        if(!ReviewHelpers::isBiddingOpen()){
            Flash::warning( trans('reviews::reviews.bidding.closed') );
            return back();
        }

        $submissions = Submission::orderBy('title')->get();
        $bids = Bid::where('user_id', Auth::id())->get();
        return view('reviews::bidding', compact( 'submissions', 'bids' ));
    }

    public function store(Request $request){

        $existing_bids = Bid::where('user_id', Auth::id())->get();

        $bids = $request->bid;

        foreach ($bids as $sid => $bid){
            if($existing_bids->contains('submission_id', $sid)){
                $b = $existing_bids->first(function ($k, $v) use ($sid){
                    return $v->submission_id == $sid;
                });
                if($b->locked){
                    continue;
                }
            } else {
                $b = new Bid();
                $b->user_id = Auth::id();
                $b->submission_id = $sid;
            }
            $b->bid = $bid;
            $b->save();
        }

        Flash::success( trans('reviews::reviews.bidding.save_successful') );

        return redirect(m_action( '\EConf\Reviews\Http\Controllers\BiddingController@show' ));

    }

}
