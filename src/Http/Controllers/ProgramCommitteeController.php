<?php

namespace EConf\Reviews\Http\Controllers;

use App\Http\Controllers\CommitteeController;

class ProgramCommitteeController extends CommitteeController{

	protected static $committee_name = 'program';
	protected static $trans_key = 'reviews::reviews.program_committee';

}
