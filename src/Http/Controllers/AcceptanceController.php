<?php

namespace EConf\Reviews\Http\Controllers;

use App\Committee;
use App\Http\Controllers\Controller;
use App\SessionType;
use Auth;
use Config;
use DB;
use Doctrine\Common\CommonException;
use EConf\Reviews\Bid;
use EConf\Reviews\Conflict;
use EConf\Reviews\ConflictException;
use EConf\Reviews\Review;
use EConf\Reviews\ReviewHelpers;
use EConf\Submissions\Submission;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Setting;
use Flash;
use stdClass;
use Eventy;

class AcceptanceController extends Controller {

    public function index() {

        $this->checkAccess();

        // Add assets
        Eventy::addFilter( 'admin.assets', function ( $val ) {
            $val[] = 'bower_components/datatables.net/js/jquery.dataTables.min.js';
            $val[] = 'bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js';
            $val[] = 'bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css';

            $val[] = 'bower_components/datatables.net-responsive/js/dataTables.responsive.min.js';
            $val[] = 'bower_components/datatables.net-responsive-bs/js/responsive.bootstrap.js';
            $val[] = 'bower_components/datatables.net-responsive-bs/css/responsive.bootstrap.min.css';
            return $val;
        }, 30, 1 );

        $submissions = Submission::select(DB::raw('submissions.*, avg(reviews.score) as score'))->leftJoin('reviews', 'submissions.id', '=', 'reviews.submission_id')->groupBy('submissions.id')->orderBy('score', 'desc')->get();

        $to_accept = ReviewHelpers::submissionsToAccept($submissions->count());

        return view( 'reviews::acceptance', compact( 'submissions', 'to_accept' ) );
    }

    private function checkAccess(){
        if(!ReviewHelpers::isAfter()){
            abort(403);
        }
    }

}
