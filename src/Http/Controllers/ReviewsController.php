<?php

namespace EConf\Reviews\Http\Controllers;

use App\Committee;
use App\Http\Controllers\Controller;
use App\Invite;
use App\SessionType;
use Auth;
use Config;
use Doctrine\Common\CommonException;
use EConf\Reviews\Bid;
use EConf\Reviews\Conflict;
use EConf\Reviews\ConflictException;
use EConf\Reviews\Review;
use EConf\Reviews\ReviewHelpers;
use EConf\Submissions\Submission;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Setting;
use Flash;
use stdClass;

class ReviewsController extends Controller {

    public function index() {
        $reviews = Review::where( 'user_id', Auth::id() )->get();
        $reviews_delegated = Review::where( 'assignee_id', Auth::id() )->get();

        return view( 'reviews::review.index', compact( 'reviews', 'reviews_delegated' ) );
    }

    public function show( $id ) {

        $review = Review::findOrFail( $id );

        if ( $review->user_id != Auth::id() ) {
            abort( 404 );
        }

        return view( 'reviews::review.show', compact( 'review' ) );

    }

    public function store( Request $request, $id ) {

        $review = Review::findOrFail( $id );

        if ( $review->user_id != Auth::id() ) {
            abort( 404 );
        }

        $rules = [];
        $attributes = [
            'comments_author' => trans('reviews::reviews.review.comments_author'),
            'comments_pc' => trans('reviews::reviews.review.comments_pc'),
        ];

        foreach (ReviewHelpers::formFields() as $field){
            $rules["field.{$field['slug']}"] = 'required';
            $attributes["field.{$field['slug']}"] = $field['name'];
        }

        foreach (['comments_author','comments_pc'] as $field){
            if(in_array($field,explode(',',Setting::get('conf-rev-form-require','')))){
                $rules[$field] = 'required';
            }
        }

        $this->validate( $request, $rules, [], $attributes);

        $data = $request->all();

        unset( $data['_token'], $data['_method'] );

        $review->data = $data;

        $review->save();

        Flash::success( trans( 'reviews::reviews.review.save_successful' ) );

        return redirect( m_action( '\EConf\Reviews\Http\Controllers\ReviewsController@index' ) );
    }

    public function delegate(Request $request){
        // IMPROVE: add subm title when validating
        $this->validate( $request, [
            'submissions' => 'required',
            'submissions.*' => 'exists:reviews,id,user_id,'.Auth::id().',score,NULL,assignee_id,NULL',
            'email' => 'required|email',
        ], [], [
            'submissions' => trans('submissions::submissions.label'),
            'submissions.*' => trans('submissions::submissions.label'),
            'email' => trans('econf.user.email'),
            'message' => trans('econf.admin.email.message')
        ] );

        $invite_data = [
            'type' => 'review',
            'reviews' => $request->submissions,
        ];

        Invite::make( $request->email, $invite_data, 'reviews::reviews.review.delegate_email', $request->message );

        Flash::success( trans('reviews::reviews.review.invite_sent') );

        return redirect(m_action('\EConf\Reviews\Http\Controllers\ReviewsController@index'));

    }

}
