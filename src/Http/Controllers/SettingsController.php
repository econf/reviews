<?php

namespace EConf\Reviews\Http\Controllers;

use App\Http\Controllers\Controller;
use App\SessionType;
use Eventy;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Setting;
use Flash;

class SettingsController extends Controller {

    public function show() {
        // Add assets
        Eventy::addFilter( 'admin.assets', function ( $val ) {
            $val[] = action( '\EConf\Reviews\Http\Controllers\AssetsController@css' );
            $val[] = action( '\EConf\Reviews\Http\Controllers\AssetsController@js' );
            return $val;
        }, 30, 1 );

        return view( 'reviews::settings' );
    }

    public function store( Request $request ) {

        // Validate request
        $this->validate( $request, [
            'setting.conf-rev-start_date' => 'date_format:Y-m-d',
            'setting.conf-rev-end_date' => 'date_format:Y-m-d|after:setting.conf-rev-start_date',
            'setting.conf-rev-bid-start_date' => 'date_format:Y-m-d',
            'setting.conf-rev-bid-end_date' => 'date_format:Y-m-d|after:setting.conf-rev-bid-start_date|before:setting.conf-rev-start_date',
            'setting.conf-rev-asgn-reviews_paper' => 'required|integer|min:1'
        ], [], [
            'setting.conf-rev-start_date' => trans('reviews::reviews.settings.start_date'),
            'setting.conf-rev-end_date' => trans('reviews::reviews.settings.end_date'),
            'setting.conf-rev-bid-start_date' => trans('reviews::reviews.settings.start_date'),
            'setting.conf-rev-bid-end_date' => trans('reviews::reviews.settings.end_date'),
            'setting.conf-rev-asgn-reviews_paper' => trans('reviews::reviews.settings.reviews_per_paper')
        ] );


        $setting = $request->setting;

        $setting['conf-rev-bid-info'] = implode( ',', array_keys( isset( $request->bidding_info ) ? $request->bidding_info : [ ] ) );

        $rev_fields = array_values( $request->get( 'rev_fields', [ ] ) );

        array_walk( $rev_fields, function ( &$val, $key ) {
            $val['name'] = trim( $val['name'] );
            $val['slug'] = Str::snake( $val['name'] );
        } );

        $setting['conf-rev-form-fields'] = serialize( $rev_fields );

        $setting['conf-rev-form-require'] = implode( ',', array_keys( $request->get( 'require', [ ] ) ) );

        foreach ( $setting as $key => $value ) {
            if ( trim( $value ) == false ) {
                Setting::forget( $key );
            } else {
                Setting::set( $key, trim( $value ) );
            }
        }

        Flash::success( trans( 'reviews::reviews.settings.success' ) );
        return redirect( m_action( '\EConf\Reviews\Http\Controllers\SettingsController@show' ) );
    }

}
