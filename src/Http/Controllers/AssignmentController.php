<?php

namespace EConf\Reviews\Http\Controllers;

use App\Committee;
use App\Http\Controllers\Controller;
use App\SessionType;
use Asset;
use Auth;
use Doctrine\Common\CommonException;
use EConf\Reviews\Assignment\Algorithm;
use EConf\Reviews\Bid;
use EConf\Reviews\Conflict;
use EConf\Reviews\ConflictException;
use EConf\Reviews\Review;
use EConf\Reviews\ReviewHelpers;
use EConf\Submissions\Submission;
use Eventy;
use Illuminate\Http\Request;

use Setting;
use Flash;
use stdClass;

class AssignmentController extends Controller {

    public function manual_show() {

        $this->confirmAccess();

        // Add assets
        Eventy::addFilter( 'admin.assets', function ( $val ) {
            $val[] = action( '\EConf\Reviews\Http\Controllers\AssetsController@css' );
            $val[] = action( '\EConf\Reviews\Http\Controllers\AssetsController@js' );
            return $val;
        }, 30, 1 );

        $subs = Submission::all();
        $revs = ProgramCommitteeController::getCommittee()->users;
        $bids = Bid::all();
        $assigns = Review::all();

        $papers_reviewer = $subs->count() * Setting::get( 'conf-rev-asgn-reviews_paper', 3 ) / $revs->count();

        $submissions = [ ];

        foreach ( $subs as $sub ) {
            $s = new stdClass();
            $s->submission = $sub;
            $s->reviewers = [ ];
            foreach ( $revs as $rev ) {
                $bid = $bids->first( function ( $key, $value ) use ( $sub, $rev ) {
                    return $value->submission_id == $sub->id && $value->user_id == $rev->id;
                } );
                if ( !is_null( $bid ) && $bid->bid == "conflict" ) {
                    continue;
                }
                $r = new stdClass();
                $r->user = $rev;

                $r->bid = $bid;
                $r->assignment = $assigns->first( function ( $key, $value ) use ( $sub, $rev ) {
                    return $value->submission_id == $sub->id && $value->user_id == $rev->id;
                } );
                /*$r->bidsCount = $bids->filter(function($v, $k) use ($rev){
                    return $v->user_id == $rev->id && $v->bid == "high";
                })->count();*/
                $r->assignmentsCount = $assigns->filter( function ( $v, $k ) use ( $rev ) {
                    return $v->user_id == $rev->id;
                } )->count();
                $s->reviewers[] = $r;

            }
            $submissions[] = $s;
        }

//        dd($submissions);

        return view( 'reviews::assignment.manual', compact( 'submissions', 'papers_reviewer' ) );
    }

    public function manual_store( Request $request ) {

        $this->confirmAccess();

        $this->storeAssignments( $request->asgn );

        Flash::success( trans( 'reviews::reviews.assignment.save_successful' ) );

        return redirect( m_action( '\EConf\Reviews\Http\Controllers\AssignmentController@manual_show' ) );
    }

    public function auto_show() {

        $this->confirmAccess();

        $algo_array = Eventy::filter('reviews.assignment.algorithms');

        $algorithms = [];

        foreach ($algo_array as $algo){
            $algorithms[] = new $algo();
        }

        return view('reviews::assignment.auto', compact( 'algorithms' ));
    }

    public function auto_algo($slug){

        $this->confirmAccess();

        // Add assets
        Eventy::addFilter( 'admin.assets', function ( $val ) {
            $val[] = action( '\EConf\Reviews\Http\Controllers\AssetsController@css' );
            return $val;
        }, 30, 1 );

        $algos = Eventy::filter('reviews.assignment.algorithms', []);

        if(!array_key_exists( $slug, $algos )){
            Flash::error( 'Non-existant algorithm (translate this)' );
            return redirect(m_action( '\EConf\Reviews\Http\Controllers\AssignmentController@auto_show' ));
        }

        $alg_class = $algos[$slug];

        /**
         * @var $algorithm Algorithm The assignment algorithm
         */
        $algorithm = new $alg_class();

        $assigns = $algorithm->assign();

//        dd($assigns);
        ksort($assigns);

        $submissions = Submission::all()->keyBy( 'id' );
        $reviewers = ProgramCommitteeController::getCommittee()->users->keyBy( 'id' );
        $bids = Bid::all();

        $existing_assignments = Review::count() > 0;

        return view('reviews::assignment.algorithm', compact( 'algorithm', 'submissions', 'reviewers', 'bids', 'assigns', 'existing_assignments' ));

    }

    public function auto_store( Request $request ) {
        $this->storeAssignments( $request->asgn );

        Flash::success( trans( 'reviews::reviews.assignment.save_successful' ) );

        return redirect( m_action( '\EConf\Reviews\Http\Controllers\AssignmentController@manual_show' ) );
    }

    /**
     * @param $asgn
     * @throws \Exception
     */
    private function storeAssignments( $asgn ) {

        if(empty($asgn)){
            $asgn = [];
        }

        $assignments = Review::all();

        foreach ( $assignments as $a ) {
            if ( isset( $asgn[$a->submission_id][$a->user_id] ) ) {
                unset( $asgn[$a->submission_id][$a->user_id] );
            } else {
                $a->delete();
            }
        }

        foreach ( $asgn as $sub_id => $revs ) {
            foreach ( array_keys( $revs ) as $rev_id ) {
                Review::create( [
                    'submission_id' => $sub_id,
                    'user_id' => $rev_id
                ] );
            }
        }
    }

    private function confirmAccess() {
        if ( !ReviewHelpers::isAssignmentPhase() ) {
            abort( 403 );
        }
    }

}
