<?php
/**
 * Created by IntelliJ IDEA.
 * User: joaoluis
 * Date: 31/05/16
 * Time: 18:41
 */

namespace EConf\Reviews\Http\Middleware;


use Closure;
use EConf\Reviews\Review;
use EConf\Reviews\ReviewHelpers;

class ReviewerMiddleware {

    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!ReviewHelpers::isReviewer()){
            return redirect(m_action('PublicController@index'));
        }

        return $next($request);
    }

}
