<?php

// Settings > Submission
Breadcrumbs::register('admin.settings.reviews', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.settings');
    $breadcrumbs->push(trans('reviews::reviews.settings.label_short'));
});

// Revision
Breadcrumbs::register('reviews', function($breadcrumbs)
{
    $breadcrumbs->push(trans('reviews::reviews.label'));
});

// Revision > Bidding
Breadcrumbs::register('reviews.bidding', function($breadcrumbs)
{
    $breadcrumbs->parent('reviews');
    $breadcrumbs->push(trans('reviews::reviews.public.bidding'));
});

// Revision > Conflicts
Breadcrumbs::register('reviews.conflict', function($breadcrumbs)
{
    $breadcrumbs->parent('reviews');
    $breadcrumbs->push(trans('reviews::reviews.conflict.label'));
});

// Revision > Assignment
Breadcrumbs::register('reviews.assignment', function($breadcrumbs)
{
    $breadcrumbs->parent('reviews');
    $breadcrumbs->push(trans('reviews::reviews.assignment.label'));
});

// Revision > Assignment > Manual
Breadcrumbs::register('reviews.assignment.manual', function($breadcrumbs)
{
    $breadcrumbs->parent('reviews.assignment');
    $breadcrumbs->push(trans('reviews::reviews.assignment.manual_short'));
});

// Revision > Assignment > Auto
Breadcrumbs::register('reviews.assignment.auto', function($breadcrumbs)
{
    $breadcrumbs->parent('reviews.assignment');
    $breadcrumbs->push(trans('reviews::reviews.assignment.auto_short'));
});

// Revision > Review
Breadcrumbs::register('reviews.review', function($breadcrumbs)
{
    $breadcrumbs->parent('reviews');
    $breadcrumbs->push(trans('reviews::reviews.review.label'), m_action( '\EConf\Reviews\Http\Controllers\ReviewsController@index' ));
});

// Revision > Review > Indiv
Breadcrumbs::register('reviews.review.show', function($breadcrumbs, $review)
{
    $breadcrumbs->parent('reviews.review');
    $breadcrumbs->push($review->submission->title);
});

// Revision > Overview
Breadcrumbs::register('reviews.overview', function($breadcrumbs)
{
    $breadcrumbs->parent('reviews');
    $breadcrumbs->push(trans('reviews::reviews.overview.label'));
});

// Revision > Overview > Indiv
Breadcrumbs::register('reviews.overview.show', function($breadcrumbs, $review)
{
    $breadcrumbs->parent('reviews.overview');
    $breadcrumbs->push($review->submission->title);
});
