<?php
/**
 * Created by IntelliJ IDEA.
 * User: joaoluis
 * Date: 30/05/16
 * Time: 18:56
 */

namespace EConf\Reviews\Assignment;


use EConf\Reviews\Bid;
use EConf\Reviews\Http\Controllers\ProgramCommitteeController;
use EConf\Submissions\Submission;
use Setting;

class Dummy extends Algorithm {

    protected $name = "Dummy Assignment";
    protected $description = "This algorithm just assigns the reviewers in order, although it avoids conflicts. This algorithm complete ignores bids and topics.";

    /**
     * Computes the assignments and return an array with the format: $assigns[sub_id] = [rev1_id, rev2_id, ..., revN_id]
     */
    public function assign() {

        $subs = Submission::all();

        $revs = ProgramCommitteeController::getCommittee()->users;

        $bids = Bid::all();

        $revs_count = $revs->count();

        $revs_paper = min( Setting::get( 'conf-rev-asgn-reviews_paper', 3 ), $revs_count);

        $assigns = [];

        $i = 0;

        foreach ($subs as $sub){
            $sr = [];
            $nr = 0;
            $ar = $revs_count;
            while($nr < $revs_paper && $ar > 0){
                $rev = $revs->get( $i % $revs_paper );
                $i++;
                $ar--;
                $bid = $bids->first( function ( $key, $value ) use ( $sub, $rev ) {
                    return $value->submission_id == $sub->id && $value->user_id == $rev->id;
                } );
                if ( !is_null( $bid ) && $bid->bid == "conflict" ) {
                    continue;
                }
                $nr++;
                $sr[] = $rev->id;
            }

            $assigns[$sub->id] = $sr;
        }

        return $assigns;
    }
}
