<?php
/**
 * Created by IntelliJ IDEA.
 * User: joaoluis
 * Date: 30/05/16
 * Time: 18:39
 */

namespace EConf\Reviews\Assignment;


use Illuminate\Support\Str;

abstract class Algorithm {

    /**
     * @var string The algorithm name.
     */
    protected $name;

    /**
     * @var string The algorithm description.
     */
    protected $description;

    public final function getSlug(){
        return Str::snake( $this->name );
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }



	/**
     * Computes the assignments and return an array with the format: $bids[sub_id] = [rev1_id, rev2_id, ..., revN_id]
     */
    public abstract function assign();

}
