<?php
/**
 * Created by IntelliJ IDEA.
 * User: joaoluis
 * Date: 30/05/16
 * Time: 18:56
 */

namespace EConf\Reviews\Assignment;


use App\User;
use EConf\Reviews\Bid;
use EConf\Reviews\Http\Controllers\ProgramCommitteeController;
use EConf\Reviews\Review;
use EConf\Submissions\Submission;
use Illuminate\Database\Eloquent\Collection;
use Setting;

class Area extends Algorithm {

    protected $name = "AREA - Article REviewer Assignment";
    protected $description = "AREA assigns first the high bids, giving preference to the reviewers and papers
                                with less high bids, assigning after the medium bids, by the same preference.
                                The remaining submissions will be assigned taking into account the topics.";


    /**
     * @var Collection The list of submissions.
     */
    private $subs;
    /**
     * @var Collection The list of reviewers.
     */
    private $revs;
    /**
     * @var Collection the list of bids.
     */
    private $bids;

    /**
     * @var Collection The original list of reviewers, to allow to easily reload it.
     */
    private $original_revs;

    /**
     * @var array The reviewers for each submission.
     */
    private $subs_revs;
    /**
     * @var array The submissions for each reviewer.
     */
    private $revs_subs;

    /**
     * @var int The number of reviews per submission.
     */
    private $reviews_per_submission;
    /**
     * @var int The number of submissions per reviewer.
     */
    private $submissions_per_reviewer;

    /**
     * Computes the assignments and return an array with the format: $assigns[sub_id] = [rev1_id, rev2_id, ..., revN_id]
     */
    public function assign() {

        $this->init();

        // Assign first the high bids and then the medium.
        $this->assign_block( 'high' );
        $this->assign_block( 'medium' );

        // There will be papers left. Let's assign them.
        // But first, let's order the reviewers list, first the ones with no assignments.
        $this->revs = $this->revs->sortBy( function ( $reviewer, $key ) {
            return empty( $this->revs_subs[$reviewer->id] ) ? 0 : count( $this->revs_subs[$reviewer->id] );
        } )->keyBy( 'id' );
        $this->assign_remaining_submissions();

        // There may still be papers left!
        if ( !$this->subs->isEmpty() ) {
            // Let's reload the reviewers list and start again!
            $this->revs = $this->original_revs;
            $this->assign_remaining_submissions();
        }

        return $this->subs_revs;
    }


    /**
     * Assigns the submissions for the passed bid value.
     *
     * @param $value string The bid value.
     */
    private function assign_block( $value ) {

        // Pick the reviewers who have $value bids. The reviewers with less $value bids will go first.
        $reviewers = $this->bids->filter( function ( Bid $bid, $key ) use ( $value ) {
            return $bid->bid == $value;
        } )->map( function ( Bid $bid, $key ) {
            return $this->revs->where( 'id', $bid->user_id )->first();
        } )->filter(function($value, $key){
            return !is_null( $value);
        })->unique( 'id' )->sortBy( function ( $reviewer, $key ) {
            return $this->bids->where( 'user_id', $reviewer->id )->count();
        } )->keyBy( 'id' );

        foreach ( $reviewers as $reviewer ) {
            // Get the submissions for which the $reviewer bidded $value.
            // The submissions with less $value bids will go first.
            $reviewer_bids = $this->bids->where( 'user_id', $reviewer->id )->where( 'bid', $value )->map( function ( Bid $bid, $key ) {
                return $this->subs->where( 'id', $bid->submission_id )->first();
            } )->filter(function($value, $key){
                return !is_null( $value);
            })->sortBy( function ( Submission $submission, $key ) {
                return $this->bids->where( 'submission_id', $submission->id )->count();
            } )->intersect( $this->subs )->keyBy( 'id' );

            foreach ( $reviewer_bids as $submission ) {
                // Assign $reviewer to $paper.
                $this->subs_revs[$submission->id][] = $reviewer->id;
                $this->revs_subs[$reviewer->id][] = $submission->id;

                // If the reviewer and/or the submission has now the desired amount of submissions/reviewers,
                // remove them from the list.
                if ( count( $this->subs_revs[$submission->id] ) >= $this->reviews_per_submission ) {
                    $this->subs->forget( $submission->id );
                }
                if ( count( $this->revs_subs[$reviewer->id] ) >= $this->submissions_per_reviewer ) {
                    $this->revs->forget( $reviewer->id );
                    break;
                }
            }
        }
    }

    /**
     * Initialize the class fields.
     *
     * They are only loaded from the database once. All the subsequent operations are performed on Collections.
     */
    private function init() {
        $this->subs = Submission::all()->keyBy( 'id' );
        $this->original_revs = ProgramCommitteeController::getCommittee()->users->keyBy( 'id' );
        $this->revs = clone $this->original_revs;
        $this->bids = Bid::all();

        $this->subs_revs = [ ];
        $this->revs_subs = [ ];

        $this->reviews_per_submission = Setting::get( 'conf-rev-asgn-reviews_paper', 3 );
        $this->submissions_per_reviewer = ceil( $this->subs->count() * $this->reviews_per_submission / $this->revs->count() );
    }

    /**
     * Assign the papers remaining on $this->papers on a per-submission basis.
     */
    private function assign_remaining_submissions() {
        foreach ( $this->subs as $submission ) {
            // Get the reviewers who can review this submission - those without conflicts of interest and who
            // aren't already reviewing it. The list will be ordered according to topic-matching.
            $reviewers = $this->revs->filter( function ( User $reviewer, $key ) use ( $submission ) {
                return $this->bids->where( 'user_id', $reviewer->id )->where( 'submission_id', $submission->id )->where( 'bid', 'conflict' )->isEmpty();
            } )->reject( function ( User $reviewer, $key ) use ( $submission ) {
                if ( !array_key_exists( $submission->id, $this->subs_revs ) ) {
                    return false;
                }
                return in_array( $reviewer->id, $this->subs_revs[$submission->id] );
            } )->sortByDesc( function ( User $reviewer, $key ) use ( $submission ) {
                $user_topics = explode( ',', $reviewer->data( 'topics', '' ) );
                $submission_topics = $submission->topics->pluck( 'id' );
                $common_topics = $submission_topics->intersect( $user_topics );
                return $common_topics->count();
            } )->keyBy( 'id' );
            foreach ( $reviewers as $reviewer ) {
                // Assign $reviewer to $submission.
                $this->subs_revs[$submission->id][] = $reviewer->id;
                $this->revs_subs[$reviewer->id][] = $submission->id;

                // If the reviewer and/or the submission has now the desired amount of submissions/reviewers,
                // remove them from the list.
                if ( count( $this->revs_subs[$reviewer->id] ) >= $this->submissions_per_reviewer ) {
                    $this->revs->forget( $reviewer->id );
                }
                if ( count( $this->subs_revs[$submission->id] ) >= $this->reviews_per_submission ) {
                    $this->subs->forget( $submission->id );
                    break;
                }

            }
        }
    }
}
