$(document).ready(function(){
    $('.reviewer').click(function(){
        var $this = $(this);
        var $checkbox = $this.find('input[type=checkbox]');

        $this.toggleClass("active");
        $checkbox.prop("checked", !$checkbox.prop("checked"));

        var revId = $this.attr('data-id');
        var $rev = $('.reviewer[data-id="'+revId+'"]');
        var currAssigns = $this.attr('data-assignments');

        if($this.hasClass('active')){
            // Assigned
            currAssigns++;
        } else {
            // Not assigned
            currAssigns--;
        }

        $rev.attr('data-assignments', currAssigns);
        $rev.find('.assignments').text(currAssigns);
    });
});
