<?php

return [
    'label' => 'Revision',
    'program_committee' => 'Program committee',
    'settings' => [
        'label' => 'Revision Settings',
        'label_short' => 'Revision',
        'phase_settings' => 'Phase settings',
        'start_date' => 'Start date',
        'end_date' => 'End date',
        'success' => 'Settings updated successfully.',
        'bidding_settings' => 'Bidding settings',
        'bidding_info_to_show' => 'Information to show on bidding',
        'assignment_settings' => 'Assignment settings',
        'reviews_per_paper' => 'Reviews per paper',
        'review_form_fields' => 'Review form fields',
        'review_form_fields_info' => 'These fields will be presented with a scale from 1 to 5. The review score will be calculated using the reviewer\'s answers and the fields\' weights.',
        'review_form_field_name' => 'Name',
        'review_form_field_weight' => 'Weight',
        'review_form_add_field' => 'Add field',
        'review_form_require' => 'Require :field',
    ],
    'dashboard_label' => 'Revisions',
    'status' => [
        'closed' => 'Closed',
        'open_until' => 'Ends on :date',
        'opens_in' => 'Opens on :date',
    ],
    'public' => [
        'phase' => 'Revision phase',
        'bidding' => 'Paper bidding',
    ],
    'bidding' => [
        'bid' => 'Bid',
        'save_successful' => 'Bidding was saved successfully.',
        'closed' => 'The bidding phase is closed.',
        'options' => [
            'high' => 'High / Very interested',
            'medium' => 'Medium / Topic knowledge',
            'low' => 'Low / No expertise',
            'conflict' => 'In conflict'
        ],
        'options_short' => [
            'high' => 'High',
            'medium' => 'Med',
            'low' => 'Low',
            'conflict' => 'Confl'
        ],
        'callout' => [
            'title' => 'Don\'t forget to bid on the submissions',
            'message' => 'You haven\'t yet bid on the submissions. Bidding will allow you to rate articles closer to your interests. Check the bidding phase closing date on the calendar below.'
        ],
    ],
    'conflict' => [
        'label' => 'Conflicts of interest',
        'submission_title' => 'Submission title',
        'author' => 'Author',
        'unknown' => 'Unknown',
        'reviewer_name' => 'Reviewer',
        'conflict' => 'Conflict',
        'add_conflict' => 'Add conflict',
        'add_conflict_success' => 'Conflict added successfully.',
        'remove_conflict_success' => 'Conflict removed successfully.',
        'type' => [
            'name' => [
                'title' => 'Similar name',
                'message' => 'The author and the reviewer have a similar name (with a similarity of :msg).'
            ],
            'org_email' => [
                'title' => 'Same organization',
                'message' => 'According to their email addresses, the author and the reviewer belong to the same organization - :msg.'
            ],
            'manual' => [
                'title' => 'Manual by reviewer',
                'message' => 'This conflict was registered manually by the reviewer.'
            ],
            'manual_pc' => [
                'title' => 'Manual by PC member',
                'message' => 'This conflict was registered manually by a Program Committee member.'
            ]
        ],
    ],
    'assignment' => [
        'label' => 'Assignment',
        'manual' => 'Manual assignment',
        'manual_short' => 'Manual',
        'auto' => 'Automatic assignment',
        'auto_short' => 'Automatic',
        'papers_reviewer' => 'In order to have :rev_ppr reviews per paper, each reviewer should review, on average, <strong>:ppr_rev</strong> papers.',
        'save_successful' => 'Assignments saved successfully.',
        'generate' => 'Generate assignments',
        'auto_about_title' => 'About the automatic assignment',
        'auto_about_message_algos' => 'This page lists the available algorithms for automatic assignment. Different algorithms may generate different results.',
        'auto_about_message_method' => 'When you click Generate, you will be shown a page with the results generated by the algorithm, without saving them. If you want to apply those assignments, you can do so on that page.',
        'auto_warning_title' => 'The existing assignments will be lost',
        'auto_warning_message' => 'There are already some saved assignments. Saving the automatic assignments will delete all existing assignments.',
        'auto_tune_message' => 'After saving you will be able to tune the assignments on the manual assignment page.',
    ],
    'review' => [
        'label' => 'Review', // Verb
        'subs_to_review' => 'Submissions to review',
        'delegated_subs' => 'Delegated submissions',
        'action' => 'Review', // Verb, to be used on buttons
        'comments' => 'Comments',
        'comments_author' => 'Comments for the author',
        'comments_pc' => 'Comments for the Program Committee',
        'fields_info' => 'Classify this submission on this criteria, with 1 being the minimum and 5 the maximum. The review score will be calculated based on these values.',
        'save_successful' => 'Review saved successfully.',
        'delegated_to' => 'Delegated to',
        'delegated_by_name' => 'Delegated by <strong>:name</strong>',
        'delegate_reviews' => 'Delegate reviews',
        'delegate' => 'Delegate', // Action
        'already_assigned' => 'The review of the submission <strong>:title</strong> is already assigned, so it will be skipped.',
        'invite_sent' => 'The invite was sent successfully.',
        'invite_title' => 'Review &ldquo;:title&rdquo;',
        'invite_text' => 'Review &ldquo;:title&rdquo; for :inviter',
        'invite_accepted' => 'The invite was accepted.',
        'delegate_email' => [
            'subject' => ':inviter asked you to review some submissions',
            'top' => ':inviter asked your help to review some submissions for :conf_name.',
        ],
    ],
    'overview' => [
        'label' => 'Overview',
        'subm_id' => 'Sub. ID',
        'subm_title' => 'Submission title',
        'subm_keywords' => 'Submission keywords',
        'reviewer' => 'Reviewer',
        'score' => 'Score',
        'review' => 'Review', // Label to column with the buttons to see the contents of the review
        'no_review_yet' => 'The reviewer hasn\'t reviewed this submission yet.',
    ],
    'email' => [
        'reviewers_all' => 'Reviewers - All',
        'authors_accepted' => 'Authors - Submission accepted',
        'authors_rejected' => 'Authors - Submission rejected',
        'vars' => [
            'score' => 'Score', // Should be identical to array['overview']['score']
            'reviews' => 'Reviews',
        ],
    ],
    'check_highlighted' => 'Check highlighted',
    'topics' => 'Topics',
    'topics_hint' => 'Choose the topics of your interest',
];
