<?php

return [
    'label' => 'Revisão',
    'program_committee' => 'Comissão de programa',
    'settings' => [
        'label' => 'Definições de revisão',
        'label_short' => 'Revisão',
        'phase_settings' => 'Definições da fase',
        'start_date' => 'Data de início',
        'end_date' => 'Data de fim',
        'success' => 'Definições atualizadas com sucesso.',
        'bidding_settings' => 'Definições de licitação',
        'bidding_info_to_show' => 'Informações a mostrar na licitação',
        'assignment_settings' => 'Definições de atribuição',
        'reviews_per_paper' => 'Revisões por artigo',
        'review_form_fields' => 'Campos do formulário de revisão',
        'review_form_fields_info' => 'Estes campos serão apresentados com uma escala de 1 a 5. A pontuação da revisão será calculada usando as respostas dos revisores e os pesos dos campos.',
        'review_form_field_name' => 'Nome',
        'review_form_field_weight' => 'Peso',
        'review_form_add_field' => 'Adicionar campo',
        'review_form_require' => 'Tornar :field obrigatório',
    ],
    'dashboard_label' => 'Revisões',
    'status' => [
        'closed' => 'Fechadas',
        'open_until' => 'Fecham a :date',
        'opens_in' => 'Abrem a :date',
    ],
    'public' => [
        'phase' => 'Fase de revisão',
        'bidding' => 'Licitação de artigos',
    ],
    'bidding' => [
        'bid' => 'Licitação',
        'save_successful' => 'A licitação foi guardada com sucesso.',
        'closed' => 'A fase de licitação está fechada.',
        'options' => [
            'high' => 'Alto / Muito interessado',
            'medium' => 'Médio / Conhecimento sobre o assunto',
            'low' => 'Baixo / Sem conhecimento',
            'conflict' => 'Em conflito'
        ],
        'options_short' => [
            'high' => 'Alto',
            'medium' => 'Méd',
            'low' => 'Baixo',
            'conflict' => 'Confl'
        ],
        'callout' => [
            'title' => 'Não se esqueça de licitar nas submissões',
            'message' => 'Ainda não licitou nas submissões. Licitar permitará que avalie artigos mais próximos dos seus interesses. Confirme a data de fecho da fase de licitação no calendário abaixo.'
        ],
    ],
    'conflict' => [
        'label' => 'Conflitos de interesse',
        'submission_title' => 'Título da submissão',
        'author' => 'Autor',
        'unknown' => 'Desconhecido',
        'reviewer_name' => 'Revisor',
        'conflict' => 'Conflito',
        'add_conflict' => 'Adicionar conflito',
        'add_conflict_success' => 'Conflito adicionado com sucesso.',
        'remove_conflict_success' => 'Conflito removido com sucesso.',
        'type' => [
            'name' => [
                'title' => 'Nome semelhante',
                'message' => 'O autor e o revisor têm um nome semelhante (com uma semelhança de :msg).'
            ],
            'org_email' => [
                'title' => 'Mesma organização',
                'message' => 'De acordo com os seus endereços de e-mail, o autor e o revisor pertencem à mesma organização - :msg.'
            ],
            'manual' => [
                'title' => 'Manual pelo revisor',
                'message' => 'Este conflito foi registado manualmente pelo utilizador.'
            ],
            'manual_pc' => [
                'title' => 'Manual por membro da CP',
                'message' => 'Este conflito foi registado manualmente por um membro da Comissão de Programa.'
            ]
        ],
    ],
    'assignment' => [
        'label' => 'Atribuição',
        'manual' => 'Atribuição manual',
        'manual_short' => 'Manual',
        'auto' => 'Atribuição automática',
        'auto_short' => 'Automática',
        'papers_reviewer' => 'Para ter :rev_ppr revisões por artigo, cada revisor deverá rever, em média, <strong>:ppr_rev</strong> artigos.',
        'save_successful' => 'Atribuições guardadas com sucesso.',
        'generate' => 'Gerar atribuições',
        'auto_about_title' => 'Sobre a atribuição automática',
        'auto_about_message_algos' => 'Esta página lista os algoritmos disponíveis para atribuição automática. Algoritmos diferentes podem gerar resultados diferentes.',
        'auto_about_message_method' => 'Quando clicar em Gerar, ser-lhe-á mostrada uma página com os resultados gerados pelo algoritmo, sem os guardar. Se quiser utilizar essas atribuições, pode fazê-lo nessa página.',
        'auto_warning_title' => 'As atribuições existentes serão perdidas',
        'auto_warning_message' => 'Já existem algumas atribuições guardadas. Guardar as atribuições automáticas irá apagar todas as atribuições existentes.',
        'auto_tune_message' => 'Depois de guardar poderá ajustar as atribuições na página de atribuição manual.',
    ],
    'review' => [
        'label' => 'Rever', // Verb
        'subs_to_review' => 'Submissões para rever',
        'delegated_subs' => 'Submissões delegadas',
        'action' => 'Rever', // Verb, to be used on buttons
        'comments' => 'Comentários',
        'comments_author' => 'Comentários para o autor',
        'comments_pc' => 'Comentários para a Comissão de Programa',
        'fields_info' => 'Classifique esta submissão nestes critérios, sendo 1 o mínimo e 5 o máximo. A pontuação da revisão será calculada com base nestes valores.',
        'save_successful' => 'Revisão guardada com sucesso.',
        'delegated_to' => 'Delegado a',
        'delegated_by_name' => 'Delegado por <strong>:name</strong>',
        'delegate_reviews' => 'Delegar revisões',
        'delegate' => 'Delegar', // Action
        'already_assigned' => 'A revisão da submissão <strong>:title</strong> já está atribuída, pelo que será saltada.',
        'invite_sent' => 'Convite enviado com sucesso.',
        'invite_title' => 'Rever &ldquo;:title&rdquo;',
        'invite_text' => 'Rever &ldquo;:title&rdquo; para :inviter',
        'invite_accepted' => 'O convite foi aceite.',
        'delegate_email' => [
            'subject' => ':inviter pediu-lhe para rever algumas submissões',
            'top' => ':inviter pediu a sua ajuda para rever algumas submissões para :conf_name.',
        ],
    ],
    'overview' => [
        'label' => 'Vista geral',
        'subm_id' => 'ID sub.',
        'subm_title' => 'Título da submissão',
        'subm_keywords' => 'Palavras-chave da submissão',
        'reviewer' => 'Revisor',
        'score' => 'Avaliação',
        'review' => 'Revisão', // Label to column with the buttons to see the contents of the review
        'no_review_yet' => 'O revisor ainda não reviu esta submissão.',
    ],
    'email' => [
        'reviewers_all' => 'Revisores - Todos',
        'authors_accepted' => 'Autores - Submissões aceites',
        'authors_rejected' => 'Autores - Submissões rejeitadas',
        'vars' => [
            'score' => 'Avaliação', // Should be identical to array['overview']['score']
            'reviews' => 'Revisões',
        ],
    ],
    'check_highlighted' => 'Marcar destacadas',
    'topics' => 'Tópicos',
    'topics_hint' => 'Escolha os tópicos do seu interesse',
];
