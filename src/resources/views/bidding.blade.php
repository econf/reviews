@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('reviews::reviews.public.bidding') }}
        </h1>
        {!! Breadcrumbs::render('reviews.bidding') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        {!! BootForm::open()->action(m_action('\EConf\Reviews\Http\Controllers\BiddingController@store')) !!}

        {!! BootForm::submit(trans('econf.actions.save'), 'btn-primary') !!}


        <div class="box box-default">
            <div class="box-body no-padding">
                <div class="table-responsive">
                    <table class="table table-bordered" style="margin-bottom: 0;">

                        <thead>
                        <tr>
                            <th>{{ trans('submissions::submissions.fields.title') }}</th>
                            @if(in_array('author', explode(',', Setting::get('conf-rev-bid-info', ''))))
                                <th>{{ trans('submissions::submissions.form.authors') }}</th>
                            @endif
                            @foreach(['keywords', 'abstract', 'document'] as $name)
                                @if(in_array($name, explode(',', Setting::get('conf-rev-bid-info', ''))))
                                    <th>{{ trans('submissions::submissions.fields.'.$name) }}</th>
                                @endif
                            @endforeach
                            @foreach(trans('reviews::reviews.bidding.options_short') as $val => $str)
                                <th class="text-center">{{ $str }}</th>
                            @endforeach
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($submissions as $subm)
                            <tr>
                                <td>{{ $subm->title }}</td>
                                @if(in_array('author', explode(',', Setting::get('conf-rev-bid-info', ''))))
                                    <td>{{ $subm->data('author')->implode('name', ', ') }}</td>
                                @endif
                                @if(in_array('keywords', explode(',', Setting::get('conf-rev-bid-info', ''))))
                                    <td>{{ $subm->data('keywords')->implode(', ') }}</td>
                                @endif
                                @if(in_array('abstract', explode(',', Setting::get('conf-rev-bid-info', ''))))
                                    <td class="text-center">
                                        <a tabindex="0" role="button" class="btn btn-xs btn-default"
                                           data-toggle="popover" data-trigger="focus"
                                           title="{{ trans('submissions::submissions.fields.abstract') }}"
                                           data-content="{{ $subm->data('abstract') }}">
                                            <span class="fa fa-eye"></span>
                                        </a>
                                    </td>
                                @endif
                                @if(in_array('document', explode(',', Setting::get('conf-rev-bid-info', ''))))
                                    <td class="text-center">
                                        <a href="{{ action('FileController@get', m_path("submissions/{$subm->getRouteKey()}.pdf")) }}"
                                           class="btn btn-xs btn-default">
                                            <span class="fa fa-download"></span>
                                        </a>
                                    </td>
                                @endif
                                @if($bids->contains('submission_id', $subm->id) and $bids->first(function($k, $v) use ($subm){ return $v->submission_id == $subm->id; })->locked)
                                    <td class="danger text-center"
                                        colspan="{{ count(trans('reviews::reviews.bidding.options')) }}">
                                        {{ trans('reviews::reviews.bidding.options.conflict') }}
                                    </td>
                                @else
                                    @foreach(($opts = trans('reviews::reviews.bidding.options_short')) as $val => $str)
                                        <td class="text-center">
                                                <input type="radio" name="bid[{{$subm->id}}]" value="{{$val}}"
                                                       @if($bids->contains('submission_id', $subm->id))
                                                       @if($bids->first(function($k, $v) use ($subm){ return $v->submission_id == $subm->id; })->bid == $val)
                                                       checked
                                                       @endif
                                                       @elseif($str == reset($opts)) checked @endif
                                                >
                                        </td>
                                    @endforeach
                                @endif


                                {{-- <td>
                                     @if($bids->contains('submission_id', $subm->id) and $bids->first(function($k, $v) use ($subm){ return $v->submission_id == $subm->id; })->locked)
                                         <span
                                             class="text-danger">{{ trans('reviews::reviews.bidding.options.conflict') }}</span>
                                     @else
                                         {!! Form::select("bid[{$subm->id}]", trans('reviews::reviews.bidding.options'), $bids->contains('submission_id', $subm->id)?$bids->first(function($k, $v) use ($subm){ return $v->submission_id == $subm->id; })->bid:'low', ['class' => 'form-control']) !!}
                                     @endif
                                 </td>--}}
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>

        {!! BootForm::submit(trans('econf.actions.save'), 'btn-primary') !!}

        {!! BootForm::close() !!}

    </section>
@endsection
