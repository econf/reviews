@if($showBiddingCallout)
    <div class="callout callout-info">
        <h4>{{ trans('reviews::reviews.bidding.callout.title') }}</h4>
        <p>{{ trans('reviews::reviews.bidding.callout.message') }}</p>
    </div>
@endif
