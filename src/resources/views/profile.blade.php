<div class="form-group">
    <label>{{ trans('reviews::reviews.topics') }}</label>
    @foreach(\App\Topic::orderBy('name')->get() as $topic)
        {!! BootForm::checkbox($topic->name, "topics[{$topic->id}]")->setOldValue(old('topics.'.$topic->id, in_array($topic->id,explode(",",Auth::user()->data("topics", "")))), false) !!}
    @endforeach
    <p class="help-block">{{ trans('reviews::reviews.topics_hint') }}</p>
</div>
