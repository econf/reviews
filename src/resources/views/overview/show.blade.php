@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('reviews::reviews.review.label') }}
            <small>{{ $review->submission->title }}</small>
        </h1>
        {!! Breadcrumbs::render('reviews.overview.show', $review) !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')


        <div class="row">

            <div class="col-md-6">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">
                            {{ $review->submission->title }}
                        </h3>
                    </div>
                    <div class="box-body">
                        <dl style="margin-bottom: 0;">
                            <dt>{{ trans('submissions::submissions.form.authors') }}</dt>
                            <dd>{{ $review->submission->data('author')->implode('name', ', ') }}</dd>
                            @if(Setting::get("conf-subm-keywords", false))
                                <dt>{{ trans('submissions::submissions.fields.keywords') }}</dt>
                                <dd>{{ $review->submission->data('keywords')->implode(', ') }}</dd>
                            @endif
                            @if(Setting::get("conf-subm-abstract", false))
                                <dt>{{ trans('submissions::submissions.fields.abstract') }}</dt>
                                <dd>{{ $review->submission->data('abstract') }}</dd>
                            @endif
                        </dl>
                    </div>
                    @if(Setting::get('conf-subm-document', false) && Storage::exists(m_path("submissions/{$review->submission->id}.pdf")))
                        <div class="box-footer">
                            <a href="{{ action('FileController@get', m_path("submissions/{$review->submission->getRouteKey()}.pdf")) }}"
                               class="btn btn-default">
                                <span class="fa fa-download"></span>
                                {{ trans('submissions::submissions.download') }}
                            </a>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row score-row">
                            <div class="col-xs-7">
                                <img src="{{ $review->user->photo }}" alt="{{ $review->user->short_name }}"
                                     class="profile-user-img img-responsive img-circle">
                                <h3 class="profile-username text-center">{{ $review->user->name }}</h3>
                            </div>
                            <div class="col-xs-5 score-container">
                                @if(empty($review->score))
                                    <div class="score-block score-block-empty">
                                        <div class="fa fa-question"></div>
                                    </div>
                                @else
                                    <div class="score-block score-block-{{ floor($review->score) }}">
                                        <span class="score-value">{{ $review->score }}</span>
                                        <span class="score-label">{{ trans('reviews::reviews.overview.score') }}</span>
                                    </div>
                                @endif
                            </div>
                        </div>

                    </div>
                    @if(empty($review->score))
                        <div class="box-body bg-gray-light border-top text-muted text-center">
                            {{ trans('reviews::reviews.overview.no_review_yet') }}
                        </div>
                    @else
                        <div class="box-body no-padding">
                            <table class="table table-responsive table-striped">
                                <tbody>
                                @foreach(\EConf\Reviews\ReviewHelpers::formFields() as $field)
                                    <tr>
                                        <td>{{ $field['name'] }}</td>
                                        <td style="font-weight: bold;">{{ $review->data("field.{$field['slug']}", 0) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            @foreach(['comments_author', 'comments_pc'] as $field)
                @unless(empty($review->data($field)))
                    <div class="col-md-6">
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    {{ trans("reviews::reviews.review.{$field}") }}
                                </h3>
                            </div>
                            <div class="box-body">
                                {{ $review->data($field) }}
                            </div>
                        </div>
                    </div>
                @endunless
            @endforeach
        </div>

    </section>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.knob').knob();
        });
    </script>
@endsection
