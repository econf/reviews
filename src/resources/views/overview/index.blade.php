@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('reviews::reviews.overview.label') }}
        </h1>
        {!! Breadcrumbs::render('reviews.overview') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box box-default">
            <div class="box-body">
                <table class="table table-bordered table-striped overview-table dt-responsive" width="100%">
                    <thead>
                    <tr>
                        <th>{{ trans('reviews::reviews.overview.subm_id') }}</th>
                        <th data-priority="1">{{ trans('reviews::reviews.overview.subm_title') }}</th>
                        <th>{{ trans('reviews::reviews.overview.subm_keywords') }}</th>
                        <th data-priority="3">{{ trans('reviews::reviews.overview.reviewer') }}</th>
                        <th data-priority="1">{{ trans('reviews::reviews.overview.score') }}</th>
                        <th data-priority="1"
                            data-orderable="false">{{ trans('reviews::reviews.overview.review') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($reviews as $review)
                        <tr>
                            <td>{{ $review->submission->id }}</td>
                            <td>
                                <a href="{{ m_action("\EConf\Submissions\Http\Controllers\AdminController@show", $review->submission) }}">{{ $review->submission->title }}</a>
                            </td>
                            <td>{{ $review->submission->data('keywords')->implode(', ') }}</td>
                            <td>{{ $review->user->short_name }}</td>
                            <td class="text-center">
                                @if(empty($review->score))
                                    <span class="fa fa-question text-muted"></span>
                                @else
                                    {{ $review->score }}
                                @endif
                            </td>
                            <td class="text-center">
                                <a href="{{ m_action("\EConf\Reviews\Http\Controllers\OverviewController@show", $review) }}"
                                   class="btn btn-xs btn-default">
                                    <span class="fa fa-eye"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </section>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.overview-table').DataTable(
                {!! json_encode(['language' => trans('econf.data_tables'), 'responsive' => ['details' => false]]) !!}
            );
        });
    </script>
@endsection
