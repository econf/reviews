@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('reviews::reviews.review.label') }}
            <small>{{ $review->submission->title }}</small>
        </h1>
        {!! Breadcrumbs::render('reviews.review.show', $review) !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        {!! BootForm::open()->action(m_action('\EConf\Reviews\Http\Controllers\ReviewsController@store', $review->id)) !!}

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">

            <div class="col-md-6">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">
                            {{ $review->submission->title }}
                        </h3>
                    </div>
                    <div class="box-body">
                        <dl style="margin-bottom: 0;">
                            <dt>{{ trans('submissions::submissions.form.authors') }}</dt>
                            <dd>{{ $review->submission->data('author')->implode('name', ', ') }}</dd>
                            @if(Setting::get("conf-subm-keywords", false))
                                <dt>{{ trans('submissions::submissions.fields.keywords') }}</dt>
                                <dd>{{ $review->submission->data('keywords')->implode(', ') }}</dd>
                            @endif
                            @if(Setting::get("conf-subm-abstract", false))
                                <dt>{{ trans('submissions::submissions.fields.abstract') }}</dt>
                                <dd>{{ $review->submission->data('abstract') }}</dd>
                            @endif
                        </dl>
                    </div>
                    @if(Setting::get('conf-subm-document', false) && Storage::exists(m_path("submissions/{$review->submission->id}.pdf")))
                        <div class="box-footer">
                            <a href="{{ action('FileController@get', m_path("submissions/{$review->submission->getRouteKey()}.pdf")) }}"
                               class="btn btn-default">
                                <span class="fa fa-download"></span>
                                {{ trans('submissions::submissions.download') }}
                            </a>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-body">
                        {{ trans('reviews::reviews.review.fields_info') }}
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-responsive table-striped">
                            <thead>
                            <tr>
                                <th></th>
                                @for($i = 1; $i <= 5; $i++)
                                    <th style="width: 2em;" class="text-center">{{ $i }}</th>
                                @endfor
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\EConf\Reviews\ReviewHelpers::formFields() as $field)
                                <tr @if($errors->has("field.{$field['slug']}")) class="danger" @endif>
                                    <td>{{ $field['name'] }}</td>
                                    @for($i = 1; $i <= 5; $i++)
                                        <td class="text-center">
                                            <input type="radio" value="{{ $i }}"
                                                   name="field[{{ $field['slug'] }}]"
                                                    @if(old("field.{$field['slug']}", $review->data("field.{$field['slug']}", 0)) == $i) checked @endif>
                                        </td>
                                    @endfor
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('reviews::reviews.review.comments') }}
                </h3>
            </div>
            <div class="box-body">
                {!! BootForm::textarea(trans('reviews::reviews.review.comments_author'), 'comments_author')->rows(2)->value(old('comments_author', $review->data('comments_author', ''))) !!}
                {!! BootForm::textarea(trans('reviews::reviews.review.comments_pc'), 'comments_pc')->rows(2)->value(old('comments_pc', $review->data('comments_pc', ''))) !!}
            </div>
        </div>

        {!! BootForm::submit(trans('econf.actions.save'), 'btn-primary') !!}

        {!! BootForm::close() !!}

    </section>
@endsection
