@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('reviews::reviews.review.label') }}
        </h1>
        {!! Breadcrumbs::render('reviews.review') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="box box-default">
            <div class="box-header">
                <h3 class="box-title">
                    {{ trans('reviews::reviews.review.subs_to_review') }}
                </h3>
            </div>
            <div class="box-body no-padding">
                <table class="table table-bordered" style="margin-bottom: 0;">

                    <thead>
                    <tr>
                        <th>{{ trans('submissions::submissions.fields.title') }}</th>
                        <th class="hidden-xs">{{ trans('submissions::submissions.form.authors') }}</th>
                        @foreach(['keywords', 'abstract', 'document'] as $name)
                            @if(Setting::get("conf-subm-{$name}", false))
                                <th class="hidden-xs @if($name == "keywords") hidden-sm @endif ">{{ trans('submissions::submissions.fields.'.$name) }}</th>
                            @endif
                        @endforeach
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($reviews as $rev)
                        <tr>
                            <td class="vert-align">
                                {{ $rev->submission->title }}
                                @if($rev->score)
                                    <span class="label label-default">{{ number_format($rev->score, 1) }}</span>
                                @endif
                                @if($rev->assignee)
                                    <span class="text-muted" style="display: block;font-size: .9em;">
                                        {!! trans('reviews::reviews.review.delegated_by_name', ['name' => $rev->assignee->short_name]) !!}
                                    </span>
                                @endif
                            </td>
                            <td class="hidden-xs vert-align">{{ $rev->submission->data('author')->implode('name', ', ') }}</td>
                            @if(Setting::get("conf-subm-keywords", false))
                                <td class="hidden-xs hidden-sm vert-align">{{ $rev->submission->data('keywords')->implode(', ') }}</td>
                            @endif
                            @if(Setting::get("conf-subm-abstract", false))
                                <td class="text-center hidden-xs vert-align">
                                    <a tabindex="0" role="button" class="btn btn-xs btn-default"
                                       data-toggle="popover" data-trigger="focus"
                                       title="{{ trans('submissions::submissions.fields.abstract') }}"
                                       data-content="{{ $rev->submission->data('abstract') }}">
                                        <span class="fa fa-eye"></span>
                                    </a>
                                </td>
                            @endif
                            @if(Setting::get("conf-subm-document", false))
                                <td class="text-center hidden-xs vert-align">
                                    <a href="{{ action('FileController@get', m_path("submissions/{$rev->submission->getRouteKey()}.pdf")) }}"
                                       class="btn btn-xs btn-default">
                                        <span class="fa fa-download"></span>
                                    </a>
                                </td>
                            @endif
                            <td class="text-center vert-align">
                                <a href="{{ m_action('\EConf\Reviews\Http\Controllers\ReviewsController@show', $rev->id) }}"
                                   class="btn btn-xs @if(empty($rev->data)) btn-primary @else btn-default @endif ">
                                    {{ trans('reviews::reviews.review.action') }}
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        @unless($reviews_delegated->isEmpty())
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">
                        {{ trans('reviews::reviews.review.delegated_subs') }}
                    </h3>
                </div>
                <div class="box-body no-padding">
                    <div class="table-responsive">
                        <table class="table table-bordered" style="margin-bottom: 0;">

                            <thead>
                            <tr>
                                <th>{{ trans('submissions::submissions.fields.title') }}</th>
                                <th class="hidden-xs">{{ trans('submissions::submissions.form.authors') }}</th>
                                @foreach(['keywords', 'abstract', 'document'] as $name)
                                    @if(Setting::get("conf-subm-{$name}", false))
                                        <th class="hidden-xs @if($name == "keywords") hidden-sm @endif ">{{ trans('submissions::submissions.fields.'.$name) }}</th>
                                    @endif
                                @endforeach
                                <th>{{ trans('reviews::reviews.review.delegated_to') }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($reviews_delegated as $rev)
                                <tr>
                                    <td>
                                        {{ $rev->submission->title }}
                                        @if($rev->score)
                                            <span class="label label-default">{{ number_format($rev->score, 1) }}</span>
                                        @endif
                                    </td>
                                    <td class="hidden-xs">{{ $rev->submission->data('author')->implode('name', ', ') }}</td>
                                    @if(Setting::get("conf-subm-keywords", false))
                                        <td class="hidden-xs hidden-sm">{{ $rev->submission->data('keywords')->implode(', ') }}</td>
                                    @endif
                                    @if(Setting::get("conf-subm-abstract", false))
                                        <td class="text-center hidden-xs">
                                            <a tabindex="0" role="button" class="btn btn-xs btn-default"
                                               data-toggle="popover" data-trigger="focus"
                                               title="{{ trans('submissions::submissions.fields.abstract') }}"
                                               data-content="{{ $rev->submission->data('abstract') }}">
                                                <span class="fa fa-eye"></span>
                                            </a>
                                        </td>
                                    @endif
                                    @if(Setting::get("conf-subm-document", false))
                                        <td class="text-center hidden-xs">
                                            <a href="{{ action('FileController@get', m_path("submissions/{$rev->submission->getRouteKey()}.pdf")) }}"
                                               class="btn btn-xs btn-default">
                                                <span class="fa fa-download"></span>
                                            </a>
                                        </td>
                                    @endif
                                    <td class="text-center">
                                        {{ $rev->user->short_name }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endunless

        <p>
            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#delegateModal">
                {{ trans('reviews::reviews.review.delegate_reviews') }}
            </button>
        </p>

    </section>

    {{-- Delegate modal --}}
    <div class="modal fade" id="delegateModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">{{ trans('reviews::reviews.review.delegate_reviews') }}</h4>
                </div>
                {!! BootForm::open()->action(m_action('\EConf\Reviews\Http\Controllers\ReviewsController@delegate')) !!}
                <div class="modal-body">
                    {!! BootForm::select(trans('submissions::submissions.label'), 'submissions', $reviews->filter(function($v, $k){ return is_null($v->score) && is_null($v->assignee_id); })->pluck('submission.title', 'id'))->multiple()->addClass('select2')->style('display:block;width:100%;') !!}
                    {!! BootForm::email(trans('econf.user.email'), 'email') !!}
                    {!! BootForm::textarea(trans('econf.admin.email.message'), 'message')->addClass('koala-simple') !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        {{ trans('econf.actions.cancel') }}
                    </button>
                    {!! BootForm::submit(trans('reviews::reviews.review.delegate'), 'btn-primary') !!}
                </div>
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@endsection
