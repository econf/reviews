<h2>{{ trans('reviews.label') }}</h2>

@foreach($reviews as $idx => $review)

    @unless($idx == 0)
        <hr>
    @endunless

    <p><strong>{{ trans('reviews.overview.score') }}</strong>: {{ number_format($reviews->score, 2) }}</p>

    <ul>
        @foreach(\EConf\Reviews\ReviewHelpers::formFields() as $field)
            <li>
                <strong>{{ $field['name'] }}</strong>:
                {{ $review->data("field.{$field['slug']}", 0) }}
            </li>
        @endforeach
    </ul>

    @unless(empty($review->data('comments_author')))
        <p>{{ $review->data('comments_author') }}</p>
    @endunless

@endforeach
