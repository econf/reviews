@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('reviews::reviews.conflict.label') }}
        </h1>
        {!! Breadcrumbs::render('reviews.conflict') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        <div class="box box-default">
            <div class="box-body no-padding">
                <div class="table-responsive">
                    <table class="table table-bordered" style="margin-bottom: 0;">

                        <thead>
                        <tr>
                            <th>{{ trans('reviews::reviews.conflict.submission_title') }}</th>
                            <th>{{ trans('reviews::reviews.conflict.author') }}</th>
                            <th>{{ trans('reviews::reviews.conflict.reviewer_name') }}</th>
                            <th>{{ trans('reviews::reviews.conflict.conflict') }}</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($conflicts as $conflict)
                            <tr>
                                <td>{{ $conflict->submission->title }}</td>
                                <td>{{ $conflict->author?$conflict->author['name']:trans('reviews::reviews.conflict.unknown') }}</td>
                                <td>{{ $conflict->reviewer->name }}</td>
                                <td>
                                    {{ $conflict->title }}
                                    <a tabindex="0" role="button" class="btn btn-xs btn-link pull-right"
                                       data-toggle="popover" data-trigger="focus"
                                       data-content="  {{ $conflict->message }}">
                                        <span class="fa fa-fw fa-info-circle"></span>
                                    </a>
                                </td>
                                <td class="text-center">
                                    @if($conflict->type == "manual_pc")
                                        {!! BootForm::open()->action(m_action('\EConf\Reviews\Http\Controllers\ConflictController@destroy'))->delete() !!}
                                        {!! BootForm::hidden('bid')->value($conflict->bid->id) !!}
                                        {!! BootForm::submit('<span class="fa fa-fw fa-times"></span>', 'btn-xs btn-danger') !!}
                                        {!! BootForm::close() !!}
                                    @elseif($conflict->type != "manual")
                                        {!! BootForm::open()->action(m_action('\EConf\Reviews\Http\Controllers\ConflictController@store')) !!}
                                        {!! BootForm::hidden('submission')->value($conflict->submission->id) !!}
                                        {!! BootForm::hidden('reviewer')->value($conflict->reviewer->id) !!}
                                        {!! BootForm::submit('<span class="fa fa-fw fa-check"></span>', 'btn-xs btn-success') !!}
                                        {!! BootForm::close() !!}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>

        {!! BootForm::open()->action(m_action('\EConf\Reviews\Http\Controllers\ConflictController@store')) !!}

        <div class="box box-default">
            <div class="box-header">
                <h4 class="box-title">{{ trans('reviews::reviews.conflict.add_conflict') }}</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-6">
                        {!! BootForm::select(trans('reviews::reviews.conflict.submission_title'), 'submission', $submissions)->addClass( 'select2' )->style( 'width:100%;' ) !!}
                    </div>
                    <div class="col-sm-6">
                        {!! BootForm::select(trans('reviews::reviews.conflict.reviewer_name'), 'reviewer', $reviewers)->addClass( 'select2' )->style( 'width:100%;' ) !!}
                    </div>
                </div>
            </div>
            <div class="box-footer">
                {!! BootForm::submit(trans('econf.actions.add'), 'btn-primary') !!}
            </div>
        </div>

        {!! BootForm::close() !!}

    </section>
@endsection
