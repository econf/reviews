@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('reviews::reviews.assignment.manual') }}
        </h1>
        {!! Breadcrumbs::render('reviews.assignment.manual') !!}
    </section>

    <!-- Main content -->
    <section class="content assignment-manual">

        @include('flash::message')

        <div class="box box-info">
            <div class="box-body">
                {!! trans('reviews::reviews.assignment.papers_reviewer', ['rev_ppr' => Setting::get('conf-rev-asgn-reviews_paper', 3), 'ppr_rev' => $papers_reviewer]) !!}
            </div>
        </div>

        {!! BootForm::open()->action(m_action('\EConf\Reviews\Http\Controllers\AssignmentController@manual_store')) !!}

        <p>{!! BootForm::submit(trans('econf.actions.save'), 'btn-primary') !!}</p>

        @foreach($submissions as $sub)
            <div class="box box-solid submission">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        {{ $sub->submission->title }}
                        <small>#{{ $sub->submission->id }}</small>
                    </h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <a href="{{ m_action('\EConf\Submissions\Http\Controllers\AdminController@show', $sub->submission) }}"
                           class="btn btn-box-tool" target="_blank">
                            <span class="fa fa-info-circle"></span>
                        </a>
                    </div>
                </div>
                <div class="box-body reviewers">
                    @foreach($sub->reviewers as $rev)
                        <article class="reviewer @unless(is_null($rev->assignment)) active @endunless "
                                 data-id="{{ $rev->user->id }}" data-assignments="{{ $rev->assignmentsCount }}">
                            <div class="name">
                                {{ $rev->user->short_name }}
                                (
                                <span class="assignments">{{ $rev->assignmentsCount }}</span>
                                )
                            </div>
                            @if(is_null($rev->bid))
                                <div class="bid none">
                                    <span class="fa fa-fw fa-question-circle"></span>
                                </div>
                            @elseif($rev->bid->bid == "low")
                                <div class="bid low">
                                    <span class="fa fa-fw fa-arrow-circle-down"></span>
                                </div>
                            @elseif($rev->bid->bid == "medium")
                                <div class="bid medium">
                                    <span class="fa fa-fw fa-minus-circle"></span>
                                </div>
                            @elseif($rev->bid->bid == "high")
                                <div class="bid high">
                                    <span class="fa fa-fw fa-arrow-circle-up"></span>
                                </div>
                            @endif
                            <input type="checkbox" name="asgn[{{$sub->submission->id}}][{{$rev->user->id}}]" value="yes"
                                   @unless(is_null($rev->assignment)) checked @endunless >
                        </article>
                    @endforeach
                </div>
            </div>
        @endforeach

        <p>{!! BootForm::submit(trans('econf.actions.save'), 'btn-primary') !!}</p>

        {!! BootForm::close() !!}

    </section>
@endsection
