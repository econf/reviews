@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('reviews::reviews.assignment.auto') }}
        </h1>
        {!! Breadcrumbs::render('reviews.assignment.auto') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        <div class="callout callout-info">
            <h4>{{ trans('reviews::reviews.assignment.auto_about_title') }}</h4>
            <p>{{ trans('reviews::reviews.assignment.auto_about_message_algos') }}</p>
            <p>{{ trans('reviews::reviews.assignment.auto_about_message_method') }}</p>
        </div>

        @foreach($algorithms as $algorithm)
            <div class="box box-solid">
                <div class="box-header">
                    <h3 class="box-title">
                        {{ $algorithm->getName() }}
                    </h3>
                </div>
                <div class="box-body">
                    {{ $algorithm->getDescription() }}
                </div>
                <div class="box-footer">
                    <a href="{{ m_action('\EConf\Reviews\Http\Controllers\AssignmentController@auto_algo', $algorithm->getSlug()) }}"
                       class="btn btn-default">
                        {{ trans('reviews::reviews.assignment.generate') }}
                    </a>
                </div>
            </div>
        @endforeach

    </section>
@endsection
