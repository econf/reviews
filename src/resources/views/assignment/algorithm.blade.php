@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ $algorithm->getName() }}
        </h1>
        {!! Breadcrumbs::render('reviews.assignment.auto') !!}
    </section>

    <!-- Main content -->
    <section class="content assignment-auto">

        @include('flash::message')

        {!! BootForm::open()->action(m_action('\EConf\Reviews\Http\Controllers\AssignmentController@auto_store')) !!}

        @foreach($assigns as $sub_id => $revs)
            <div class="box box-solid submission">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        {{ $submissions->get($sub_id)->title }}
                        <small>#{{ $sub_id }}</small>
                    </h3>
                    <div class="box-tools pull-right">
                        <a href="{{ m_action('\EConf\Submissions\Http\Controllers\AdminController@show', $submissions->get($sub_id)) }}"
                           class="btn btn-box-tool" target="_blank">
                            <span class="fa fa-info-circle"></span>
                        </a>
                    </div>
                </div>
                <div class="box-body reviewers">
                    @foreach($revs as $rev_id)
                        <article class="reviewer active">
                            <div class="name">
                                {{ $reviewers->get($rev_id)->short_name }}
                            </div>
                            <?php $bid = $bids->first( function ( $key, $value ) use ( $sub_id, $rev_id ) {
                                return $value->submission_id == $sub_id && $value->user_id == $rev_id;
                            } ); ?>
                            @if(is_null($bid))
                                <div class="bid none">
                                    <span class="fa fa-fw fa-question-circle"></span>
                                </div>
                            @elseif($bid->bid == "low")
                                <div class="bid low">
                                    <span class="fa fa-fw fa-arrow-circle-down"></span>
                                </div>
                            @elseif($bid->bid == "medium")
                                <div class="bid medium">
                                    <span class="fa fa-fw fa-minus-circle"></span>
                                </div>
                            @elseif($bid->bid == "high")
                                <div class="bid high">
                                    <span class="fa fa-fw fa-arrow-circle-up"></span>
                                </div>
                            @endif
                            <input type="checkbox" name="asgn[{{$sub_id}}][{{$rev_id}}]" value="yes" checked>
                        </article>

                    @endforeach
                </div>
            </div>
        @endforeach

        @if($existing_assignments)

            <div class="callout callout-warning">
                <h4>{{ trans('reviews::reviews.assignment.auto_warning_title') }}</h4>
                <p>{{ trans('reviews::reviews.assignment.auto_warning_message') }}</p>
            </div>

        @endif

        <div class="callout callout-info">
            <p>{{ trans('reviews::reviews.assignment.auto_tune_message') }}</p>
        </div>

        {!! BootForm::submit(trans('econf.actions.save'), 'btn-primary') !!}

        {!! BootForm::close() !!}

    </section>
@endsection
