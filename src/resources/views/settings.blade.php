@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('reviews::reviews.settings.label') }}
        </h1>
        {!! Breadcrumbs::render('admin.settings.reviews') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        {!! BootForm::open()->action(m_action('\EConf\Reviews\Http\Controllers\SettingsController@store'))->post() !!}

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('reviews::reviews.settings.phase_settings') }}
                </h3>
            </div>
            <div class="box-body">

                {!! setting_input(trans('reviews::reviews.settings.start_date'), 'conf-rev-start_date', 'date') !!}
                {!! setting_input(trans('reviews::reviews.settings.end_date'), 'conf-rev-end_date', 'date') !!}

            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('reviews::reviews.settings.bidding_settings') }}
                </h3>
            </div>
            <div class="box-body">
                {!! setting_input(trans('reviews::reviews.settings.start_date'), 'conf-rev-bid-start_date', 'date') !!}
                {!! setting_input(trans('reviews::reviews.settings.end_date'), 'conf-rev-bid-end_date', 'date') !!}

                <div class="form-group">
                    <label class="control-label">{{ trans('reviews::reviews.settings.bidding_info_to_show') }}</label>

                    <div class="checkbox">
                        <label>

                            <input type="checkbox" name="bidding_info[author]" value="yes"
                                   @if(in_array('author', explode(',', Setting::get('conf-rev-bid-info', '')))) checked @endif>
                            {{ ucfirst(trans('submissions::submissions.form.author_info')) }}

                        </label>
                    </div>

                    @foreach(['keywords', 'abstract', 'document'] as $name)

                        <div class="checkbox">
                            <label>

                                <input type="checkbox" name="bidding_info[{{ $name }}]" value="yes"
                                       @if(in_array($name, explode(',', Setting::get('conf-rev-bid-info', '')))) checked @endif>
                                {{ ucfirst(trans('submissions::submissions.fields.'.$name)) }}

                            </label>
                        </div>

                    @endforeach

                </div>
            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('reviews::reviews.settings.assignment_settings') }}
                </h3>
            </div>
            <div class="box-body">
                {!! setting_input(trans('reviews::reviews.settings.reviews_per_paper'), 'conf-rev-asgn-reviews_paper', 'text', 3)->type('number')->step(1)->min(1) !!}
            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('reviews::reviews.settings.review_form_fields') }}
                </h3>
            </div>
            <div class="box-body">
                {{ trans('reviews::reviews.settings.review_form_fields_info') }}
            </div>
            <div class="box-body no-padding">
                <table class="table review_fields">
                    <thead>
                    <tr>
                        <th>{{ trans('reviews::reviews.settings.review_form_field_name') }}</th>
                        <th style="width: 6em;">{{ trans('reviews::reviews.settings.review_form_field_weight') }}</th>
                        <th style="width: 3em;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(\EConf\Reviews\ReviewHelpers::formFields() as $idx => $field)
                        <tr>
                            <td>
                                {!! BootForm::text(trans('reviews::reviews.settings.review_form_field_name'), "rev_fields[{$idx}][name]")->hideLabel()->value($field['name']) !!}
                            </td>
                            <td>
                                {!! BootForm::text(trans('reviews::reviews.settings.review_form_field_weight'), "rev_fields[{$idx}][weight]")->type('number')->value($field['weight'])->min(0)->hideLabel() !!}
                            </td>
                            <td class="action">
                                <button type="button" class="btn btn-sm btn-danger"><span class="fa fa-trash"></span>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <button type="button" class="btn btn-default btn-sm" id="reviewFormAddField">
                    <span class="fa fa-plus"></span>
                    {{ trans('reviews::reviews.settings.review_form_add_field') }}
                </button>
            </div>
            <div class="box-body">
                @foreach(['comments_author','comments_pc'] as $field)
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="require[{{ $field }}]" value="yes"
                                   @if(old("require[{{ $field }}]", in_array($field,explode(',',Setting::get('conf-rev-form-require',''))))) checked @endif>
                            {{ trans('reviews::reviews.settings.review_form_require', ['field' => trans("reviews::reviews.review.{$field}")]) }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>

        {!! BootForm::submit(trans('econf.actions.update'), 'btn-primary') !!}

        {!! BootForm::close() !!}

    </section>

@endsection

@section('scripts')
    <script>
        function template(idx) {
            var tpl = '<tr>' +
                      '<td>' +
                      '{!! BootForm::text(trans('reviews::reviews.settings.review_form_field_name'), 'rev_fields[\'+idx+\'][name]')->hideLabel() !!}' +
                      '</td>' +
                      '<td>' +
                      '{!! BootForm::text(trans('reviews::reviews.settings.review_form_field_weight'), 'rev_fields[\'+idx+\'][weight]')->type('number')->value(1)->min(0)->hideLabel() !!}' +
                      '</td>' +
                      '<td class="action">' +
                      '<button type="button" class="btn btn-sm btn-danger delete-field"><span class="fa fa-trash"></span></button>' +
                      '</td>' +
                      '</tr>';
            return tpl;
        }

        var idx = {{ count(Setting::get('conf-rev-form-fields', [])) }};

        $(document).ready(function () {
            $("#reviewFormAddField").click(function () {
                $(".review_fields tbody").append(template(idx));
                idx++;
            });

            if ($('.review_fields tbody').children().length == 0) {
                $("#reviewFormAddField").click();
            }

            $('.review_fields').on('click', '.delete-field', function (e) {
                $(this).parents('tr').eq(0).remove();
            });
        });

    </script>
@endsection
