@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('submissions::submissions.acceptance.label') }}
        </h1>
        {!! Breadcrumbs::render('admin.conference.acceptance') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include("flash::message")

        {!! BootForm::open()->action(m_action('\EConf\Submissions\Http\Controllers\AdminController@accept')) !!}

        <div class="box box-default">
            <div class="box-body">
                <table class="table table-bordered table-striped acceptance-table dt-responsive" width="100%">
                    <thead>
                    <tr>
                        <th>{{ trans('submissions::submissions.fields.id') }}</th>
                        <th data-priority="1">{{ trans('submissions::submissions.fields.title') }}</th>
                        <th>{{ trans('submissions::submissions.fields.keywords') }}</th>
                        <th data-priority="1">{{ trans('reviews::reviews.overview.score') }}</th>
                        <th data-priority="1"
                            data-orderable="false">{{ trans('submissions::submissions.acceptance.accept') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($submissions as $idx => $submission)
                        <tr @if($idx < $to_accept) class="success" @endif>
                            <td>{{ $submission->id }}</td>
                            <td>
                                <a href="{{ m_action("\EConf\Submissions\Http\Controllers\AdminController@show", $submission) }}">{{ $submission->title }}</a>
                                @if($submission->accepted)
                                    <span class="text-success fa fa-check-circle"></span>
                                @endif
                            </td>
                            <td>{{ $submission->data('keywords')->implode(', ') }}</td>
                            <td class="text-center">
                                @if(empty($submission->score))
                                    <span class="fa fa-question text-muted"></span>
                                @else
                                    {{ $submission->score }}
                                @endif
                            </td>
                            <td class="text-center">
                                <input type="checkbox" name="accept[{{ $submission->id }}]" @if($idx < $to_accept) data-should-accept @endif @if($submission->accepted) checked @endif>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        {!! BootForm::submit(trans('econf.actions.save'), 'btn-primary') !!}

        <button type="button" class="btn btn-default" id="checkHighlighted">{{ trans('reviews::reviews.check_highlighted') }}</button>

        {!! BootForm::close() !!}

    </section>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            var table = $('.acceptance-table').DataTable(
                {!! json_encode(['language' => trans('econf.data_tables'), 'paging' => false, 'responsive' => ['details' => false], 'order' => [3, 'desc']]) !!}
            );

            $('#checkHighlighted').click(function(){
                $('input[data-should-accept]').prop('checked', true);
            });
        });
    </script>
@endsection
