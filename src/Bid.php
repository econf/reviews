<?php

namespace EConf\Reviews;

use App\Traits\TenantableTrait;
use App\User;
use EConf\Submissions\Submission;
use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    use TenantableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'submission_id',
        'bid',
    ];

    protected $casts = [
        'locked' => 'boolean',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function submission(){
        return $this->belongsTo( Submission::class );
    }

}
