<?php

namespace EConf\Reviews;

use App\Committee;
use App\User;
use Auth;
use Carbon\Carbon;
use Date;
use EConf\Reviews\Assignment\Area;
use EConf\Reviews\Assignment\Dummy;
use EConf\Reviews\Http\Controllers\ProgramCommitteeController;
use EConf\Submissions\Submission;
use EConf\Submissions\SubmissionHelpers;
use Eventy;
use Flash;
use Illuminate\Support\ServiceProvider;
use Setting;

class ReviewsServiceProvider extends ServiceProvider {
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot() {
        // Load routes
        if ( !$this->app->routesAreCached() ) {
            require __DIR__ . '/Http/routes.php';
        }

        // Load breadcrumbs
        require __DIR__ . '/Http/breadcrumbs.php';

        // Load views
        $this->loadViewsFrom( __DIR__ . '/resources/views', 'reviews' );

        // Load translations
        $this->loadTranslationsFrom( __DIR__ . '/resources/lang', 'reviews' );

        // Publish migrations
        $this->publishes( [
            __DIR__ . '/database/migrations/' => database_path( 'migrations' )
        ], 'migrations' );


        $this->addAdminMenu();

        $this->addAdminContent();

        $this->addFileLogic();

        $this->registerAssignmentAlgorithms();

        $this->addEmails();

        $this->addInvites();

        $this->addTopicsLogic();

        $this->overrideAcceptance();

    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register() {
        //
    }

    private function addAdminMenu() {
        // Add to menu
        Eventy::addAction( 'admin.menu', function ( $menu ) {
            $isReviewer = ReviewHelpers::isReviewer();
            if ( Committee::in( 'program' ) || $isReviewer ) {
                $menu->raw( trans( 'reviews::reviews.label' ), [ 'class' => 'header' ] );
                if ( ReviewHelpers::isBiddingOpen() ) {
                    $menu->add( trans( 'reviews::reviews.public.bidding' ), m_action( '\EConf\Reviews\Http\Controllers\BiddingController@show' ) )
                         ->prepend( '<span class="fa fa-file-text"></span>' );
                }

                if ( $isReviewer && ReviewHelpers::isOpen() ) {
                    $menu->add( trans( 'reviews::reviews.review.label' ), m_action( '\EConf\Reviews\Http\Controllers\ReviewsController@index' ) )
                         ->prepend( '<span class="fa fa-star-half-o"></span>' );
                }

                if ( Committee::in( 'program', true ) ) {

                    if ( ReviewHelpers::isAssignmentPhase() || ReviewHelpers::isOpen() || ReviewHelpers::isAfter() ) {
                        $menu->add( trans( 'reviews::reviews.overview.label' ), m_action( '\EConf\Reviews\Http\Controllers\OverviewController@index' ) )
                             ->prepend( '<span class="fa fa-table"></span>' );
                    }

                    $menu->add( trans( 'reviews::reviews.conflict.label' ), m_action( '\EConf\Reviews\Http\Controllers\ConflictController@show' ) )
                         ->prepend( '<span class="fa fa-warning"></span>' );

                    if ( ReviewHelpers::isAssignmentPhase() ) {
                        $item = $menu->add( trans( 'reviews::reviews.assignment.label' ), '#' )
                                     ->prepend( '<span class="fa fa-chain"></span>' );
                        $item->add( trans( 'reviews::reviews.assignment.manual' ), m_action( '\EConf\Reviews\Http\Controllers\AssignmentController@manual_show' ) )
                             ->prepend( '<span class="fa fa-hand-paper-o"></span>' );
                        if ( !empty( Eventy::filter( 'reviews.assignment.algorithms', [ ] ) ) ) {
                            $item->add( trans( 'reviews::reviews.assignment.auto' ), m_action( '\EConf\Reviews\Http\Controllers\AssignmentController@auto_show' ) )
                                 ->prepend( '<span class="fa fa-magic"></span>' );
                        }
                    }
                }

            }
        }, 300, 1 );


        // Add to organization
        Eventy::addAction( 'admin.menu.organizer', function ( $menu ) {
            $menu->add( trans( 'reviews::reviews.program_committee' ), m_action( '\EConf\Reviews\Http\Controllers\ProgramCommitteeController@index' ) )
                 ->prepend( '<span class="fa fa-star-o"></span>' );
        }, 30, 1 );

        // Add to settings menu
        Eventy::addAction( 'admin.menu.settings', function ( $menu ) {
            $menu->add( trans( 'reviews::reviews.settings.label_short' ), m_action( '\EConf\Reviews\Http\Controllers\SettingsController@show' ) )
                 ->prepend( '<span class="fa fa-star-half-o"></span>' );
        }, 30, 1 );
    }

    private function addAdminContent() {
        // Add to dashboard
        Eventy::addFilter( 'dashboard.data', function ( $data ) {

            $text = trans( 'reviews::reviews.status.closed' );

            if ( ReviewHelpers::isOpen() ) {
                $text = trans( 'reviews::reviews.status.open_until', [ 'date' => Date::parse( Setting::get( 'conf-rev-end_date' ) )->format( trans( 'econf.date.dayMonth' ) ) ] );
            } else if ( Setting::has( 'conf-rev-start_date' ) && Carbon::now()->lt( Carbon::parse( Setting::get( 'conf-rev-start_date' ) ) ) ) {
                $text = trans( 'reviews::reviews.status.opens_in', [ 'date' => Date::parse( Setting::get( 'conf-rev-start_date' ) )->format( trans( 'econf.date.dayMonth' ) ) ] );
            }

            $progress = 100;
            if ( ReviewHelpers::isOpen() ) {
                // calc progress
                $reviews = Review::all();
                $rev_count = $reviews->count();
                if ( $rev_count > 0 ) {
                    $rev_done = $reviews->filter( function ( $val ) {
                        return !empty( $val->score );
                    } )->count();

                    $progress = ( $rev_done / $rev_count ) * 100;
                } else {
                    $progress = 0;
                }


            } else if ( Setting::has( 'conf-rev-start_date' ) && Carbon::now()->lt( Carbon::parse( Setting::get( 'conf-rev-start_date' ) ) ) ) {
                $progress = 0;
            }

            $data[] = [
                'label' => trans( 'reviews::reviews.dashboard_label' ),
                'value' => number_format( $progress, 2 ),
                'small' => '%',
                'icon' => 'fa fa-star-o',
                'class' => 'bg-orange',
                'footer' => $text,
                'progress' => $progress,
            ];
            return $data;
        }, 30, 1 );

        Eventy::addFilter( 'dashboard.calendar', function ( $calendar ) {
            if ( Setting::has( 'conf-rev-start_date' ) && Setting::has( 'conf-rev-end_date' ) ) {
                $calendar[] = [
                    'label' => trans( 'reviews::reviews.public.phase' ),
                    'start' => Date::parse( Setting::get( 'conf-rev-start_date' ) ),
                    'end' => Date::parse( Setting::get( 'conf-rev-end_date' ) ),
                ];
            }
            if ( Setting::has( 'conf-rev-bid-start_date' ) && Setting::has( 'conf-rev-bid-end_date' ) ) {
                $calendar[] = [
                    'label' => trans( 'reviews::reviews.public.bidding' ),
                    'start' => Date::parse( Setting::get( 'conf-rev-bid-start_date' ) ),
                    'end' => Date::parse( Setting::get( 'conf-rev-bid-end_date' ) ),
                ];
            }
            return $calendar;
        }, 30, 1 );

        // Add to index
        Eventy::addAction( 'dashboard.html', function () {
            if ( Committee::in( 'program' ) ) {
                $showBiddingCallout = ReviewHelpers::isBiddingOpen() && Bid::where( 'user_id', \Auth::id() )->count() == 0;
                echo view( 'reviews::dashboard', compact( 'showBiddingCallout' ) );
            }
        }, 30, 0 );
    }

    /**
     * Register the logic to be added to FileController
     */
    private function addFileLogic() {

        Eventy::addFilter( 'file.can', function ( $value, $path ) {
            if ( $path[0] == 'submissions' ) {
                if ( Committee::in( 'program' ) && ReviewHelpers::isBiddingOpen() && in_array( 'document', explode( ',', Setting::get( 'conf-rev-bid-info', '' ) ) ) ) {
                    return true;
                }
                $last_idx = count( $path ) - 1;
                $id = Submission::decodeHiddenId( explode( '.', $path[$last_idx] )[0] );
                if ( ReviewHelpers::isOpen() && Review::where( 'user_id', \Auth::id() )->where( 'submission_id', $id )->count() > 0 ) {
                    return true;
                }
            }
            return $value;
        }, 30, 2 );

    }

    private function registerAssignmentAlgorithms() {
        Eventy::addFilter( 'reviews.assignment.algorithms', function ( $algos ) {

            if ( config('app.debug') ) {
                $dummy = new Dummy();
                $algos[$dummy->getSlug()] = get_class( $dummy );
            }

            $area = new Area();
            $algos[$area->getSlug()] = get_class( $area );

            return $algos;
        }, 30, 1 );
    }

    private function addEmails() {

        function emails_from_reviews( $reviews ) {
            $emails = [ ];
            foreach ( $reviews as $review ) {
                $rid = 'r' . $review->user_id;
                if ( empty( $emails[$rid] ) ) {
                    $a = [
                        'id' => 'r' . $review->user->id,
                        'name' => $review->user->short_name,
                        'email' => $review->user->email,
                        'toString' => $review->user->short_name . ' - ' . $review->user->email,
                    ];
                    $emails[$a['id']] = $a;
                }

            }
            return $emails;
        }

        // Add groups
        Eventy::addFilter( 'admin.email.groups', function ( $groups ) {
            if ( Committee::in( 'program', true ) ) {

                $revGroups = [
                    ProgramCommitteeController::emailGroup(),
                    [
                        'id' => 'reviewers-all',
                        'name' => trans( 'reviews::reviews.email.reviewers_all' ),
                        'emails' => function () {
                            $reviews = Review::all();
                            return emails_from_reviews( $reviews );
                        }
                    ],
                ];

                foreach ( $revGroups as $revGroup ) {
                    $groups[$revGroup['id']] = $revGroup;
                }
            }

            return $groups;

        }, 30, 1 );

        // Add var names
        Eventy::addFilter( 'admin.email.vars', function ( $vars ) {
            return array_merge( $vars, trans( 'reviews::reviews.email.vars' ) );
        }, 30, 1 );

    }

    private function addInvites() {
        Eventy::addFilter( 'invite.messages.review', function ( $rules, $invite ) {

            foreach ( $invite->data( 'reviews', [ ] ) as $id ) {
                $review = Review::find( $id );

                if ( !empty($review) ) {
                    $text = "Review &ldquo;{$review->submission->title}&rdquo;";

                    $rules[] = [
                        'title' => trans( 'reviews::reviews.review.invite_title', [ 'title' => $review->submission->title ] ),
                        'text' => trans( 'reviews::reviews.review.invite_text', [
                            'title' => $review->submission->title,
                            'inviter' => $review->user->short_name
                        ] )
                    ];
                }

            }

            return $rules;
        }, 30, 2 );

        Eventy::addAction( 'invite.accept.review', function ( $invite ) {

            foreach ( $invite->data( 'reviews', [ ] ) as $id ) {
                $review = Review::find( $id );


                if ( !empty($review) ) {
                    if ( !empty( $review->assignee_id ) ) {
                        Flash::warning( trans( 'reviews::reviews.review.already_assigned', [ 'title' => $review->submission->title ] ) );
                        continue;
                    }

                    $review->assignee_id = $review->user_id;
                    $review->user_id = Auth::id();

                    $review->save();
                }

            }

            Flash::success( trans( 'reviews::reviews.review.invite_accepted' ) );

        }, 30, 1 );
    }

    private function addTopicsLogic() {

        Eventy::addAction( 'me.profile.show', function ( $user ) {
            if ( Committee::in( 'program' ) ) {
                echo view( 'reviews::profile' );
            }
        }, 30, 1 );

        Eventy::addAction( 'me.profile.save', function ( $user, $request ) {
            if ( Committee::in( 'program' ) ) {
                $topics = implode( ",", array_keys( $request->topics ) );
                $data = $user->data;
                $data = array_merge( compact( 'topics' ), $data );
                $user->data = $data;
            }
        }, 30, 2 );
    }

    private function overrideAcceptance() {

        Eventy::addFilter( 'submissions.acceptance_open', function ($value){
            return ReviewHelpers::isAfter();
        }, 300, 1);

    }

}
